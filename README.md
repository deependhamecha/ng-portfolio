# NgPortfolio

## Development server

Run `ng serve --proxy-config proxy.config.json --host 0.0.0.0` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --deploy-url="/acdn-portfolio/views/" --base-href="/portfolio/" --prod --aot=true`

Getting error of const variable then run following: 

```
ng build --deploy-url="/acdn-portfolio/views/" --base-href="/portfolio/" --dev --aot=true

```


## Update

When updating with `npm update`, it will throw an error of **gifffer**. It throws error because, we have used our custom **gifffer** and haven't published on npm. For resolving, just delete `gifffer` from `node_modules` and paste ![gifffer](https://github.com/InstaDevPrivateLimited/Gifffer) repository to `node_modules`.

  Note : Changes of custom gifffer were minor, only one line was added to `gifffer.js`. Check the GIF logo on default and custom. A text **GIF** was added.