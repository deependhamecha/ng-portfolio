export class PortfolioGrid {
    constructor(public name?: string,
                public typography?: any,
                public softwares?: any,
                public technologies?: any,
                public color_schemes?: any,
                public inspiration?: string,
                public asset?: any,
                public asset_url?: string,
                public thumbnail?: any,
                public thumbnail_url?: string,
                public id?: number,
                public type?: number,
                public reposition?: any,
                public reposition_url?: string) {
    }

    // These were added later, that is the reason why it could not be included in constructor instantiation.
    public device: string;
    public megapixel: string;
    public aperture: string;
    public sensor: string;
    public resolution: string;
    public iso: string;
    public shutterspeed: string;

    public getDevice(): string {
        return this.device;
    }

    public setDevice(value: string) {
        this.device = value;
    }

    public getMegapixel(): string {
        return this.megapixel;
    }

    public setMegapixel(value: string) {
        this.megapixel = value;
    }

    public getAperture(): string {
        return this.aperture;
    }

    public setAperture(value: string) {
        this.aperture = value;
    }

    public getSensor(): string {
        return this.sensor;
    }

    public setSensor(value: string) {
        this.sensor = value;
    }

    public getResolution(): string {
        return this.resolution;
    }

    public setResolution(value: string) {
        this.resolution = value;
    }

    public getIso(): string {
        return this.iso;
    }

    public setIso(value: string) {
        this.iso = value;
    }

    public getShutterspeed(): string {
        return this.shutterspeed;
    }

    public setShutterspeed(value: string) {
        this.shutterspeed = value;
    }

    public reset() : void {
        this.name = '';
        this.typography = [];
        this.softwares = [];
        this.technologies = [];
        this.color_schemes = [];
        this.inspiration = '';
        this.asset = null;
        this.asset_url = '';
        this.thumbnail = null;
        this.thumbnail_url = '';
        this.id = undefined;
        this.type = undefined;
        this.reposition = null;
        this.reposition_url = '';
    }
}