import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ViewChild, AfterViewInit } from '@angular/core';

import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Angular2Gridster
import { GridsterModule } from 'angular2gridster';


import { FileDropModule } from 'ngx-file-drop';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { GridComponent } from './grid/grid.component';

// NGX-Bootstrap
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';

// Interceptor
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// Interceptor Service
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { PortfolioManagerComponent } from './portfolio-manager/portfolio-manager.component';

// Progess Bar [ngx-progressbar]
import { NgProgressModule, NgProgress } from '@ngx-progressbar/core';

import { UploadService } from './services/upload.service';

import { BsModalService } from 'ngx-bootstrap/modal';

import { CommonService } from './services/common.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PortfolioManagerSidebarService } from './services/portfolio-manager-sidebar.service';

import { ViewPortfolioComponent } from './view-portfolio/view-portfolio.component';

import { RouterModule, Routes } from '@angular/router';
import { EditPortfolioComponent } from './edit-portfolio/edit-portfolio.component';
import { ProfileManagerComponent } from './profile-manager/profile-manager.component';
import { CommonErrorModalComponent } from './common-error-modal/common-error-modal.component';
import { HeaderComponent } from './header/header.component';
import { ViewGridComponent } from './view-grid/view-grid.component';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';

import { Select2Module } from 'ng2-select2';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';

import { WebCamModule } from 'ack-angular-webcam';
import { AcdnWebCamComponent } from './acdn-web-cam/acdn-web-cam.component';
import { HttpModule } from '@angular/http';
import { WebCamModalComponent } from './web-cam-modal/web-cam-modal.component';
import { VideoUrlModalComponent } from './video-url-modal/video-url-modal.component';
import { FormatWithCommaPipe } from './format-with-comma.pipe';
import { ViewSidebarComponent } from './view-sidebar/view-sidebar.component';

import { ReCaptchaModule } from 'angular2-recaptcha';
import { DoubleTapDirective } from './double-tap.directive';

import { ClipboardModule } from 'ngx-clipboard';

import { EmbedVideo, EmbedVideoService } from 'ngx-embed-video';

import { CookieService } from 'ngx-cookie-service';
import { ListingPortfolioComponent } from './listingportfolio/listingportfolio.component';
import { AcdnHeaderComponent } from './acdn-header/acdn-header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';

import { ContactPopupComponent } from './contact-popup/contact-popup.component';
import { BottomScrollDirective } from './bottom-scroll.directive';

import { ShareButtonsModule } from '@ngx-share/buttons';

var appRoutes: Routes =  [
  {
    path: '',
    component: ListingPortfolioComponent
  },
  {
    path: ':username/edit',
    component: EditPortfolioComponent
    // canActivate: [AuthGuardService]
  },
  {
    path: ':username',
    component: ViewPortfolioComponent
  },
  {
    path: 'errorpage/404', component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    PortfolioManagerComponent,
    SidebarComponent,
    ViewPortfolioComponent,
    EditPortfolioComponent,
    ProfileManagerComponent,
    CommonErrorModalComponent,
    HeaderComponent,
    ViewGridComponent,
    RichTextEditorComponent,
    AcdnWebCamComponent,
    WebCamModalComponent,
    VideoUrlModalComponent,
    FormatWithCommaPipe,
    ViewSidebarComponent,
    ListingPortfolioComponent,
    AcdnHeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ContactPopupComponent,
    DoubleTapDirective,
    BottomScrollDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FileDropModule,
    HttpModule,
    HttpClientModule,
    GridsterModule,
    ModalModule.forRoot(),
    NgProgressModule,
    RouterModule.forRoot(appRoutes),
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    TabsModule.forRoot(),
    Select2Module,
    WebCamModule,
    ReCaptchaModule,
    ClipboardModule,
    EmbedVideo.forRoot(),
    HttpClientJsonpModule,  // for linkedin and tumblr share counts
    ShareButtonsModule.forRoot()
  ],
  providers: [
	  {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    BsModalService,
    UploadService,
    PortfolioManagerSidebarService,
    NgProgress,
    { provide: 'Window', useValue: window },
    { provide: 'Document', useValue: document },
    EmbedVideoService,
    CookieService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    CommonErrorModalComponent,
    WebCamModalComponent,
    VideoUrlModalComponent,
    ContactPopupComponent
  ]
})
export class AppModule { }
