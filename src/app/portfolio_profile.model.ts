export class PortfolioProfile {

    public name: string = '';
    public portfolio_name: string = '';
    public institution: string = '';
    public company: string = '';
    public location: string = '';
    public profileimg: string = '';
    public coverimg: string = '';
    public description: string = '';

    constructor() {  }

    public reset() : void {
        this.name = '';
        this.portfolio_name = '';
        this.institution = '';
        this.company = '';
        this.institution = '';
        this.location = '';
        this.profileimg = '';
        this.coverimg = '';
        this.description = '';
    }
}