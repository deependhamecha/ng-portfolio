import {  Directive,
          Output,
          EventEmitter,
          ElementRef,
          HostListener,
          Input
} from '@angular/core';

@Directive({
  selector: '[doubleTap]'
})
export class DoubleTapDirective {

  @Input('delay') delay:number = 300;
  @Output('doubleTapped') doubleTappedEmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private el: ElementRef) {

  }

  @HostListener('touchend')
  doubledTap() {
    let now = new Date().getTime();
    // The first time this will make delta a negative number.
    // let lastTouch = $(this).data('lastTouch') || now + 1;

    let lastTouch = this.el.nativeElement.getAttribute('lastTouch') || now + 1;
    let delta = now - lastTouch;
    if (delta < this.delay && 0 < delta) {
      // After we detect a doubletap, start over.
      // $(this).data('lastTouch', null);
      this.el.nativeElement.setAttribute('lastTouch', null);

      this.doubleTapped(event);

    } else {
      // $(this).data('lastTouch', now);
      this.el.nativeElement.setAttribute('lastTouch', now);
    }
  }

  doubleTapped(event) {
    this.doubleTappedEmit.emit();
  }

}
