import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatWithComma'
})
export class FormatWithCommaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.split('|').join(', ');
  }

}
