import {  Component,
          OnInit,
          ComponentFactoryResolver,
          ViewChild,
          ViewContainerRef,
          Input,
          Output,
          ChangeDetectionStrategy,
          ChangeDetectorRef,
          OnDestroy,
          Renderer2,
          ElementRef,
          EventEmitter
} from '@angular/core';

import { NgForm } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import { UploadService } from '../services/upload.service';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';

import { PortfolioProfile } from '../portfolio_profile.model';
import { PortfolioGrid } from '../portfolio_grid.model';

import { Subject, Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { ContactPopupComponent } from '../contact-popup/contact-popup.component';

@Component({
  selector: 'profile-manager',
  templateUrl: './profile-manager.component.html',
  styleUrls: ['./profile-manager.component.css']
})
export class ProfileManagerComponent implements OnInit, OnDestroy {

  @ViewChild('_contactPopupContainer', { read: ViewContainerRef })
  _contactPopupContainer: ViewContainerRef;

  @Input('isContactMobileWrapperShown') isContactMobileWrapperShown: boolean;
  
  @Output('updatePortfolioProfile') _updatePortfolioProfile: EventEmitter<any> = new EventEmitter<any>();
  
  profileEmailConfirmComponentRef;
  profileEmailConfirmComponentInstance;

  bsModalRef: BsModalRef;

  private _paramsSub;

  isCookieSet: boolean = false;
  username: string;
  portfolioProfile = {};

  availability_master = ['Not Availabile', 'Full Time', 'Part Time', 'Contract', 'Remote'];

  constructor(private factoryResolver: ComponentFactoryResolver,
              private uploadService: UploadService,
              private modalService: BsModalService,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private ren: Renderer2,
              private el: ElementRef,
              private r: Router,
              private cookieService: CookieService) {

    this._paramsSub = this.route.params.subscribe( (param) => {

      this.username = param['username'];
      this.getProfileInfo(param['username']);
    });

        // Check Login Token
    if( this.cookieService.get('_token') !== undefined &&
        this.cookieService.get('_token') !== null &&
        this.cookieService.get('_token') !== '') {

        this.uploadService.checkAuthStatus(this.cookieService.get('_token'), 'student').subscribe((res) => {
          this.isCookieSet = true;
          this.uploadService.clapStatus(this.username).subscribe(res => {
            if(res['status'] === true) {
              this.portfolioProfile['clapped'] = true;
            } else {
              this.portfolioProfile['clapped'] = false;
            }
          } , err => {});
        }, err => {});
    }
  }

  ngOnInit() {
  }

  openContactModal(partNumber) {
    // Create Modal Component
    let componentFactory = this.factoryResolver.resolveComponentFactory(ContactPopupComponent);
    this._contactPopupContainer.clear();
    this.profileEmailConfirmComponentRef = this._contactPopupContainer.createComponent(componentFactory);
    this.profileEmailConfirmComponentInstance = this.profileEmailConfirmComponentRef.instance;
    // console.log('profileEmailConfirmComponentInstance : ', this.profileEmailConfirmComponentInstance);
    // this.profileEmailConfirmComponentInstance = this.profileEmailConfirmComponentInstance.getImageEmit.subscribe(event => {
    // });
    this.profileEmailConfirmComponentInstance.partNumber = partNumber;
  }

  getEmailActionForm(form: NgForm) {
    console.log("form : ", form);
  }

  portfolioName;

  getProfileInfo(username) {

    this.uploadService.getProfileInfo(username).subscribe(
      (res) => {
        if(res['status']) {
          console.log('PROFILE INNNNNNFO : ', res['payload']['username']);
          this.portfolioName = res['payload']['name'];
          this.portfolioProfile['name'] = res['payload']['name'];
          this.portfolioProfile['portfolio_name'] = res['payload']['portfolio_name'];
          this.portfolioProfile['username'] = res['payload']['username'];

          // Set Portfolio Name to Title bar
          document.title = this.portfolioProfile['portfolio_name'];

          this.portfolioProfile['institution'] = res['payload']['institution'];
          this.portfolioProfile['company'] = res['payload']['company'];
          this.portfolioProfile['location'] = res['payload']['location'];
          this.portfolioProfile['profileimg'] = res['payload']['profileimg'];
          this.portfolioProfile['coverimg'] = res['payload']['coverimg'];
          this.portfolioProfile['claps'] = res['payload']['claps'];
          this.portfolioProfile['views'] = res['payload']['views'];
          this.portfolioProfile['availability'] = "Availabilty Status : "+this.availability_master[res['payload']['availability_id'] - 1];

          // Set Cover Image as background with css
          this.ren.setStyle(this.el.nativeElement.querySelector('.profile-cover-wrapper'), 'background-image', 'url('+this.portfolioProfile['coverimg']+')');
          this.portfolioProfile['description'] = res['payload']['description'];

          this.updatePortfolioProfile(this.portfolioProfile);
        } else {
          this.r.navigate(['/']);
        }
      },
      (error) => {

        this.r.navigate(['/']);
      }
    );
  }

  /**
   * Emits an object of PortfolioProfile
   * @param data : PortfolioProfile
   */
  updatePortfolioProfile(data: any) {
    this._updatePortfolioProfile.emit(data);
  }


  clap(username: string, clapImg, clapCount) {

    if(this.isCookieSet) {
      this.uploadService.clap(username).subscribe( (res) => {
        if(res['status'] === true) {

          if(res['payload']['clap'] === true) {
            // Change Logo to Royal Blue
            let filename = clapImg.getAttribute('src').split("/").slice(0, -1).join("/")+"/clap_royalblue.svg";
            this.ren.removeAttribute(clapImg, 'src');
            this.ren.setAttribute(clapImg, 'src', filename);

            // Change Views Count
            clapCount.innerHTML = parseInt(clapCount.innerHTML)+1;
          } else {
            // Change Logo to White
            let filename = clapImg.getAttribute('src').split("/").slice(0, -1).join("/")+"/clap_white.svg";
            this.ren.removeAttribute(clapImg, 'src');
            this.ren.setAttribute(clapImg, 'src', filename);

            // Change Views Count
            clapCount.innerHTML = parseInt(clapCount.innerHTML)-1;
          }

        } else {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = res['message'];
        }
      }, (err) => {
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Something went wrong while updating Content.';
      });
    } else {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'Please Login!';
      // $('#new-login-signup-modal').modal('show');
    }
  }

  // openResumeModal() {
  //   // Create Modal Component
  //   let componentFactory = this.factoryResolver.resolveComponentFactory(ProfileEmailConfirmModalComponent);
  //   this._profileEmailConfirmModalContainer.clear();
  //   this.profileEmailConfirmComponentRef = this._profileEmailConfirmModalContainer.createComponent(componentFactory);
  //   this.profileEmailConfirmComponentInstance = this.profileEmailConfirmComponentRef.instance;
  //   // console.log('profileEmailConfirmComponentInstance : ', this.profileEmailConfirmComponentInstance);
  //   // this.profileEmailConfirmComponentInstance = this.profileEmailConfirmComponentInstance.getImageEmit.subscribe(event => {
  //   // });
  // }

  ngOnDestroy() {
    if(this._paramsSub != undefined && this._paramsSub != null) {
      this._paramsSub.unsubscribe();
    }
  }

}
