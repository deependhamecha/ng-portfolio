import {
    Component,
    ViewChild,
    AfterViewInit,
    ElementRef
} from '@angular/core';

import { NgForm } from '@angular/forms';

import { FileDropModule, UploadEvent, UploadFile } from 'ngx-file-drop';

import { HttpClient } from '@angular/common/http';

// Bootstrap
// import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

// import { UrlPopupModalComponent } from './url-popup-modal/url-popup-modal.component';

// import { NgProgress } from '@ngx-progressbar/core';


// JQuery
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {

  }
}
