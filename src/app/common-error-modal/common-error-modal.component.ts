import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-common-error-modal',
  templateUrl: './common-error-modal.component.html',
  styleUrls: ['./common-error-modal.component.css']
})
export class CommonErrorModalComponent implements OnInit {

  public message: string;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }



}
