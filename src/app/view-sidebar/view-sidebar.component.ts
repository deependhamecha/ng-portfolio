import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-view-sidebar',
  templateUrl: './view-sidebar.component.html',
  styleUrls: ['./view-sidebar.component.css']
})
export class ViewSidebarComponent implements OnInit {

  @Input('navToggleVal')
  navToggleVal: boolean;

  @Input('isShownForDesktop')
  isShownForDesktop: boolean;

  @Output('navToggle')
  navToggleEmit: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  navToggle() {
    this.navToggleEmit.emit();
  }

}
