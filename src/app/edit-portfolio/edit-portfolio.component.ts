import {
  Component,
  ViewChild,
  AfterViewInit,
  ElementRef,
  ViewContainerRef,
  Output
} from '@angular/core';

import { NgForm } from '@angular/forms';

import { FileDropModule, UploadEvent, UploadFile } from 'ngx-file-drop';

import { HttpClient } from '@angular/common/http';

// Bootstrap
import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

// import { UrlPopupModalComponent } from './url-popup-modal/url-popup-modal.component';

import { UploadService } from '../services/upload.service';

import { NgProgress } from '@ngx-progressbar/core';
import { ModalDirective } from 'ngx-bootstrap';

import { PortfolioProfile } from '../portfolio_profile.model';

declare var window: Window;

@Component({
  selector: 'app-edit-portfolio',
  templateUrl: './edit-portfolio.component.html',
  styleUrls: ['./edit-portfolio.component.css']
})
export class EditPortfolioComponent {

  @ViewChild('_webCamModalContainer', { read: ViewContainerRef })
  webCamModalContainer: ViewContainerRef;

  @ViewChild('_videoUrlModalContainer', { read: ViewContainerRef })
  videoUrlModalContainer: ViewContainerRef;

  isActive = false;
  navToggleVal = false;
  rightContainerAnimate = true;
  optionsMenuToggle = false;

  portfolioProfile: PortfolioProfile;

  constructor(private el: ElementRef,
              private modalService: BsModalService,
              private progressService: NgProgress,
              private uploadService: UploadService) {

    // this.http.request(document.location).subscribe(response => console.log(response));
    var req = new XMLHttpRequest();
    req.open('GET', window.location+"", false);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase();
    console.log("HEADERS : ", headers);
  }

  menuToggle() {
    this.isActive = !this.isActive;
  }

  navToggle() {
    this.navToggleVal = !this.navToggleVal;
  }

  optionsMenu() {
    this.optionsMenuToggle = !this.optionsMenuToggle;
  }

  /**
  *   Update Portfolio Profile
  */
  updatePortfolioProfile(data) {
    this.portfolioProfile = data;
  }
}
