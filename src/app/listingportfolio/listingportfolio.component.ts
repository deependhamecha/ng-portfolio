  import {    Component,
            OnInit,
            AfterViewInit,
            Renderer2,
            ElementRef,
            ViewChild,
            ComponentFactoryResolver,
            ViewContainerRef
} from '@angular/core';

import { UploadService } from '../services/upload.service';

import { ContactPopupComponent } from '../contact-popup/contact-popup.component';

import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CookieService } from 'ngx-cookie-service';

declare var $: any;

@Component({
  selector: 'listing-portfolio',
  templateUrl: './listingportfolio.component.html',
  styleUrls: ['./listingportfolio.component.css']
})
export class ListingPortfolioComponent implements OnInit, AfterViewInit {

  rowData: Array<any> = [];

  // Sorting Lists
  sortings: Array<any> = [
    'Highest Claps',
    'Highest Views'
  ];

  // 1 = featured, 2 = claps, 3 = views
  currentSort = 1;

  scrolledNumber: number = 1;
  nextPortfoliosMessage: string;
  
  // 1 = default, 2 = searched, 3 = sorted
  listingType: number = 1;

  // 
  sortedType: number = 1;
  isCookieSet: boolean = false;

  showBackBtn: boolean = true;

  windowInnerWidth: number;

  email: string;
  password: string;

  constructor(  private uploadService: UploadService,
                private ren: Renderer2,
                private factoryResolver: ComponentFactoryResolver,
                private modalService: BsModalService,
                private el: ElementRef,
                private cookieService: CookieService) {

    // Check Login Token
    if( this.cookieService.get('_token') !== undefined &&
      this.cookieService.get('_token') !== null &&
      this.cookieService.get('_token') !== '') {

      this.uploadService.checkAuthStatus(this.cookieService.get('_token'), 'student').subscribe((res) => {
        this.isCookieSet = true;
        this.getPortfolioProfiles(this.scrolledNumber);
      }, (err) => {
        this.isCookieSet = false;
        this.cookieService.delete('_token', '/');
        this.cookieService.delete('laravel_session', '/');
        this.cookieService.delete('XSRF-TOKEN', '/');
        this.getPortfolioProfiles(this.scrolledNumber);
      });
    } else {
      this.isCookieSet = false;
      this.cookieService.delete('_token', '/');
      this.cookieService.delete('laravel_session', '/');
      this.cookieService.delete('XSRF-TOKEN', '/');
      this.getPortfolioProfiles(this.scrolledNumber);
    }

    this.windowInnerWidth = window.innerWidth;
  }

  ngOnInit() {
  }

  @ViewChild('navBar') navBar: ElementRef;
  @ViewChild('giftWrapper') giftWrapper: ElementRef;
  @ViewChild('cardsCollection') cardsCollection: ElementRef;

  isGiftCardShown: boolean = false;

  ngAfterViewInit() {

    // Gift Wrapper Ad Show
    // Navbar
    setTimeout( () => {
      this.isGiftCardShown = true;
    }, 1000);

    $(document).ready( () => {
      $(window).scroll( () => {
        if(this.isGiftCardShown) {
          console.log(this.isGiftCardShown);
          this.isGiftCardShown = false;
        }
      });
    });

    // nav-script.js
    $(document).ready(function(){$('.nav-nest').fadeOut(0);$('#nest-main').fadeIn();$('.nav-nest-trigger').on('click',function(){$(this).closest('.nav-nest').fadeOut(0);var nestToOpen=$(this).attr('target-nest');$(nestToOpen).fadeIn();});$('.nav-nest-back').on('click',function(){var nestToOpen=$(this).attr('target-nest');$(this).closest('.nav-nest').fadeOut(0);$(nestToOpen).fadeIn();});});

    // slidenav.js
    jQuery(document).ready(function($){function morphDropdown(element){this.element=element;this.mainNavigation=this.element.find('.main-nav');this.mainNavigationItems=this.mainNavigation.find('.has-dropdown');this.dropdownList=this.element.find('.dropdown-list');this.dropdownWrappers=this.dropdownList.find('.dropdown');this.dropdownItems=this.dropdownList.find('.content');this.dropdownBg=this.dropdownList.find('.bg-layer');this.mq=this.checkMq();this.bindEvents();}
    morphDropdown.prototype.checkMq=function(){var self=this;return window.getComputedStyle(self.element.get(0),'::before').getPropertyValue('content').replace(/'/g,"").replace(/"/g,"").split(', ');};morphDropdown.prototype.bindEvents=function(){var self=this;this.mainNavigationItems.mouseenter(function(event){self.showDropdown($(this));}).mouseleave(function(){setTimeout(function(){if(self.mainNavigation.find('.has-dropdown:hover').length==0&&self.element.find('.dropdown-list:hover').length==0)self.hideDropdown();},50);});this.dropdownList.mouseleave(function(){setTimeout(function(){(self.mainNavigation.find('.has-dropdown:hover').length==0&&self.element.find('.dropdown-list:hover').length==0)&&self.hideDropdown();},50);});this.mainNavigationItems.on('touchstart',function(event){var selectedDropdown=self.dropdownList.find('#'+$(this).data('content'));if(!self.element.hasClass('is-dropdown-visible')||!selectedDropdown.hasClass('active')){event.preventDefault();self.showDropdown($(this));}});this.element.on('click','.nav-trigger',function(event){event.preventDefault();self.element.toggleClass('nav-open');});};morphDropdown.prototype.showDropdown=function(item){this.mq=this.checkMq();if(this.mq=='desktop'){var self=this;var selectedDropdown=this.dropdownList.find('#'+item.data('content')),selectedDropdownHeight=selectedDropdown.innerHeight(),selectedDropdownWidth=selectedDropdown.children('.content').innerWidth(),selectedDropdownLeft=item.offset().left+ item.innerWidth()/2 - selectedDropdownWidth/2;
    this.updateDropdown(selectedDropdown,parseInt(selectedDropdownHeight),selectedDropdownWidth,selectedDropdownLeft);this.element.find('.active').removeClass('active');selectedDropdown.addClass('active').removeClass('move-left move-right').prevAll().addClass('move-left').end().nextAll().addClass('move-right');item.addClass('active');if(!this.element.hasClass('is-dropdown-visible')){setTimeout(function(){self.element.addClass('is-dropdown-visible');},10);}}};morphDropdown.prototype.updateDropdown=function(dropdownItem,height,width,left){this.dropdownList.css({'-moz-transform':'translateX('+ left+'px)','-webkit-transform':'translateX('+ left+'px)','-ms-transform':'translateX('+ left+'px)','-o-transform':'translateX('+ left+'px)','transform':'translateX('+ left+'px)','width':width+'px','height':height+'px'});this.dropdownBg.css({'-moz-transform':'scaleX('+ width+') scaleY('+ height+')','-webkit-transform':'scaleX('+ width+') scaleY('+ height+')','-ms-transform':'scaleX('+ width+') scaleY('+ height+')','-o-transform':'scaleX('+ width+') scaleY('+ height+')','transform':'scaleX('+ width+') scaleY('+ height+')'});};morphDropdown.prototype.hideDropdown=function(){this.mq=this.checkMq();if(this.mq=='desktop'){this.element.removeClass('is-dropdown-visible').find('.active').removeClass('active').end().find('.move-left').removeClass('move-left').end().find('.move-right').removeClass('move-right');}};morphDropdown.prototype.resetDropdown=function(){this.mq=this.checkMq();if(this.mq=='mobile'){this.dropdownList.removeAttr('style');}};var morphDropdowns=[];if($('.cd-morph-dropdown').length>0){$('.cd-morph-dropdown').each(function(){morphDropdowns.push(new morphDropdown($(this)));});var resizing=false;updateDropdownPosition();$(window).on('resize',function(){if(!resizing){resizing=true;(!window.requestAnimationFrame)?setTimeout(updateDropdownPosition,300):window.requestAnimationFrame(updateDropdownPosition);}});function updateDropdownPosition(){morphDropdowns.forEach(function(element){element.resetDropdown();});resizing=false;};}});

    // Portfolio Cards
    $(document).ready(function() {

      $(window).scroll(function() {
          if ($(this).scrollTop() > 50 ) {
              $('.scrolltop:hidden').stop(true, true).fadeIn();
          } else {
              $('.scrolltop').stop(true, true).fadeOut();
          }
      });
      $(function()
          {
              $(".scroll").click(
          function(){
              $("html,body").animate({scrollTop:0},
                  "1000");return false;})});

      $('.phonemodalOnSubmitData').hide();
      $('.emailmodalOnSubmitData').hide();
      $('.messagemodalOnSubmitData').hide();
      $('.search-btn').on('click' , function(e) {
           e.preventDefault();
      });
      $('.filter-btn').on('click' , function(e) {
           e.preventDefault();
      });
      $('.sort-btn').on('click' , function(e) {
           e.preventDefault();
      });

      let el1 = $('.views-button');
      let el2 = $('.claps-button');

      $('.views-button, .claps-button').on('DOMSubtreeModified',function() {

          console.log(el1.width()+" "+el2.width());
          if(el1.width() != el2.width()) {
              if(el1.width() > el2.height()) {
              el2.width(el1.width());
            } else {
              el1.width(el2.width());
            }
          }
      });
      var wid=$(window).width();
      var height=$(window).height();

    //   if(wid>786) {
    //     $('.merge-btn-wrap').hover(function() {
    //       $(this).find('.merge-btn-icon').addClass('mergeBtnIconHover');
    //       $(this).find('.merge-btn-label').addClass('mergeBtnLabelHover');
    //     },
    //     function() {
    //       $(this).find('.merge-btn-icon').removeClass('mergeBtnIconHover');
    //       $(this).find('.merge-btn-label').removeClass('mergeBtnLabelHover');
    //     });
    // }
    // else{
    //   $('.merge-btn-wrap').click(function() {
    //     $(this).find('.merge-btn-icon').addClass('mergeBtnIconHover');
    //     $(this).find('.merge-btn-label').addClass('mergeBtnLabelHover');
    //   });
    // }

    var wid_search;
    $('.border-animate').focusin(function() {
      // $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
      // $('.search-btn').css({'top':"-3px",'left':"-3px"});
      if($('.search-input').val()){
          // $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
          // $('.search-btn').css({'top':"-3px",'left':"-3px"});
      }
      $(this).css('width', '25%');
      wid_search=$('.border-animate').width();
      $('.filter-btn').css('left' , (wid_search+(0.08*wid_search))+"px");
      $('.sort-btn').css('left' , (wid_search+(0.24*wid_search))+"px");
      session1=1;
      searchclose();
    });
    $('.border-animate').focusout(function() {
      console.log('focusing out');
      $(this).css('width', '');
      session1=0;
      wid_search=$('.border-animate').width();
      if(wid_search==0){
          $('.border-animate').css('border',"");
          $('.search-btn').css({'top':"",'left':""});
          $('.filter-btn').css('left' , "");
          $('.sort-btn').css('left' , "");
      }
      if($('.search-input').val()) {
          // $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
          // $('.search-btn').css({'top':"-3px",'left':"-3px"});
      }
    });
    var c=0;
    var session1=0;
    var session2=0;
    var session3=0;
    $('.search-btn').hover(
      function(){
          c++;
          console.log(c);
          if(c>1 && $('.search-input').hasClass("search-input-expand")){
              $('.border-animate').css('border',"");
              $('.search-btn').css({'top':"",'left':""});
          }

          $('.border-animate').focus();

          $(this).parent().find('.search-input').addClass('search-input-expand');
          session1=1;
          searchclose();

          $(this).parent().find('.search-input').focus();

          if(wid_search!=0) {
              $('.filter-btn').css('left' , (wid_search+(0.08*wid_search))+"px");
              $('.sort-btn').css('left' , (wid_search+(0.24*wid_search))+"px");
          }

          setTimeout(function() {
            if((+$('.search-input').css('opacity'))==1 && !$('.border-animate').hasClass('draw')) {
                // $('.border-animate').addClass('draw');
                // $(".draw").bind('mouseenter mouseleave');
            }
          }, 0);
          $('.search-input').hover(
            function(){
                $(this).focus();
                $('.border-animate').css('border',"transparent");
                $('.search-btn').css({'top':"",'left':""});
            },
            function(){
                if($(this).val()) {
                    $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
                    $('.search-btn').css({'top':"-3px",'left':"-3px"});
                }
                else{
                  $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
                  $('.search-btn').css({'top':"-3px",'left':"-3px"});
                }
            }
          );
      },
      function() {
        $('.border-animate').css({'border':"3px solid #444bd6",'border-radius':"35px"});
        $('.search-btn').css({'top':"-3px",'left':"-3px"});
        if(!$('.search-input').is(":focus") || !(+$('.search-input').css('opacity')==1)) {
            $('.border-animate').focusout();
            session1=0;
            if(+($('.search-input').css('opacity'))==0 && $('.border-animate').hasClass('draw')) {
                $('.border-animate').removeClass('draw');
                console.log("222");
            }
        }
      });
      $('.filter-btn').hover(
        function(){
          if($('.search-input').val() && (!$('.search-input').hasClass("search-input-expand") || $('.border-animate').focusout())){
              session1=0;
          }
          $('.filter-list').addClass("filter-list-expand");
          $('.filter-list-item').addClass("filter-list-item-expand");
          $('.sort-btn').css("left","33%");
          session2=1;
          filterclose();

          $('.filter-list').hover(
            function() {
                $('.filter-list').addClass("filter-list-expand");
                $('.filter-list-item').addClass("filter-list-item-expand");
                $('.sort-btn').css("left","33%");
                session2=1;
                filterclose();
                console.log(session2);
            },
            function() {
            }
          );
        },
        function() {

          if( ! $(this).is(':hover') ) {
              $('.filter-list-item').removeClass("filter-list-item-expand");
              $('.filter-list').removeClass("filter-list-expand");
          }
        }
      );
    //   $('html').click(function() {
    //       if($('.filter-list').hasClass("filter-list-expand")) {
    //           $('.filter-list-item').removeClass("filter-list-item-expand");
    //               $('.filter-list').removeClass("filter-list-expand");
    //               if(session1==0){
    //                   $('.sort-btn').css("left","");
    //               }
    //               session2=0;
    //       }
    //       if($('.sort-list').hasClass("sort-list-expand")) {
    //           $('.sort-list-item').removeClass("sort-list-item-expand");
    //               $('.sort-list').removeClass("sort-list-expand");
    //               session3=0;
    //       }
    //   });
    //   $('.sort-btn').hover(
    //     function() {
    //       if($('.search-input').val() && (!$('.search-input').hasClass("search-input-expand") || $('.border-animate').focusout())){
    //           session1=0;
    //       }
    //       $('.sort-list').addClass("sort-list-expand");
    //       $('.sort-list-item').addClass("sort-list-item-expand");
    //       session3=1;
    //       sortclose();
    //       $('.sort-list').hover(
    //         function(){
    //             $('.sort-list').addClass("sort-list-expand");
    //             $('.sort-list-item').addClass("sort-list-item-expand");
    //             session3=1;
    //             sortclose();
    //         },
    //         function() {
    //         }
    //       );
    //   },
    //   function() {
    //     if( ! $(this).is(':hover') ) {
    //         $('.sort-list-item').removeClass("sort-list-item-expand");
    //         $('.sort-list').removeClass("sort-list-expand");
    //     }
    //   });
              function searchclose() {
                $('.filter-list-item').removeClass("filter-list-item-expand");
                $('.filter-list').removeClass("filter-list-expand");
                if(session1==0){
                    $('.sort-btn').css("left","");
                }
                $('.sort-list-item').removeClass("sort-list-item-expand");
                $('.sort-list').removeClass("sort-list-expand");
              }
              function filterclose() {
                $('.border-animate').focusout();
                $('.sort-list-item').removeClass("sort-list-item-expand");
                $('.sort-list').removeClass("sort-list-expand");
                $('.sort-btn').css("left","33%");
              }
              function sortclose() {
                $('.border-animate').focusout();
                $('.filter-list-item').removeClass("filter-list-item-expand");
                $('.filter-list').removeClass("filter-list-expand");
                if(session1==0){
                    $('.sort-btn').css("left","");
                }
              }
              $(".scroll").click(function() {
                $('.border-animate').focusout();
                $('.filter-list-item').removeClass("filter-list-item-expand");
                $('.filter-list').removeClass("filter-list-expand");
                $('.sort-list-item').removeClass("sort-list-item-expand");
                $('.sort-list').removeClass("sort-list-expand");
              });

              $('.search-btn-mob').click(function() {
                  $('.search-input-mob').css("opacity","1");
                  $('.search-input-mob').focus();
                  $('.search-input-mob-black').css("width",wid+"px");
                  console.log(wid+"px");
                  $('.search-input-mob-black').css("height",height+"px");
                  $('.search-input-mob-black').css("display","block");
                  $('.search-input-mob-black').css("opacity",".4");
                  $('html').css("overflow-y","hidden");
                  // $('body').css("opacity","0.5");
              });
              $('.search-input-mob').focusout(function() {
                  $('.search-input-mob').css("opacity","");
                  $('.search-input-mob-black').css("width","");
                  $('.search-input-mob-black').css("height","");
                  $('.search-input-mob-black').css("display","");
                  $('.search-input-mob-black').css("opacity","");
                  $('html').css("overflow-y","");
                  // $('body').css("opacity","");
              });
            if($('.search-input-mob').is(":focus")) {
              $('.search-btn-mob').click(function() {
                  $('.search-input-mob').css("opacity","");
                  console.log("biiibibib");
              });
            }

    });

    function sizechange() {
        var width=$('.card-image-top').width();
        var height=$('.card-image-top').height();

        $('.lower-right-triangle').css('border-left', width+"px solid transparent");
        $('.lower-right-triangle').css('border-bottom', height+"px solid #fff");
        $('.person-image').css('width', 0.22*width+"px");
        $('.person-image').css('height', 0.22*width+"px");
    }

    $('document').ready(function(){
        sizechange();
    });

    $(window).resize(function() {
        sizechange();
    });

    // Login Modal Start
    // $("#validate_submit").on('click',function(e) {
    //   e.preventDefault();
    //   var name = $('#full-name').val();
    //   var email = $('#s-email').val();
    //   var password = $('#s-password').val();
    //   var userurl = "/students/usersignup";
      
    //   if($("#signup-tab-form")[0].checkValidity())
    //   {
    //     $.ajax({
    //                   type: 'POST',
    //                   url: userurl,
    //                   data: {"_token": "{{ csrf_token() }}","name":name,"email":email,"password":password},
    //                   dataType: 'html',
    //                   success: function(data){
    //   data=   JSON.parse(data);
    //                         if(data[1]=='invalid')
    //                         {
    //                           $('#new-login-signup-modal').modal('hide');
    //                           swal(data[2]);
    //                         }
    //                         else if(data[1]=='exist')
    //                         {
    //                           $('#signup-error').text(data[2]);
    //                         }
    //                         else if(data[1]=='valid')
    //                         {
    //                           $('#new-login-signup-modal').modal('hide');
    //                           // window.location.href ='/student/home';
    //                         }
    //                     }
    //                   });
    //   }
    // });

    // $("#login-submit").on('click',function(e){
    //   e.preventDefault();
    //   var email = $('#email').val();
    //   var password = $('#password').val();
    //   var userurl = "/students/userlogin";
    //   console.log(userurl);
    //   if($("#login-tab-form")[0].checkValidity())
    //   {
    //     $.ajax({
    //       type: 'POST',
    //       url: userurl,
    //       data: {"_token": "{{ csrf_token() }}","email":email,"password":password},
    //       dataType: 'html',
    //       success: function(data) {
    //         data= JSON.parse(data);
    //         if(data[1]=='invalid')
    //         {
    //           $('#new-login-signup-modal').modal('hide');
    //           swal(data[2]);
    //         }
    //         else if(data[1]=='exist')
    //         {
    //           // $('#email').remove('small');
    //           // emailerror
    //           $('#emailerror').text(data[2]);
    //         }
    //         else if(data[1]=='valid')
    //         {
    //           $('#new-login-signup-modal').modal('hide');
    //           // window.location.href ='/student/home';
    //         }
    //       }
    //     });
    //   }
    // });
    // Login Modal End
  }

  getPortfolioProfiles(offset: number) {

    if( this.cookieService.get('_token') !== undefined &&
        this.cookieService.get('_token') !== null &&
        this.cookieService.get('_token') !== '') {
      this.uploadService.getPortfolioProfiles(offset).subscribe(
        (res) => {
          if( res['status'] && (res['payload']['total'] > 0) ) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });
            this.showBackBtn = true;
            this.activeContentType.nativeElement.innerHTML = 'Featured';
          }
          else {
            if((res['payload']['total'] == 0)) {
              this.showBackBtn = true;
              // this.nextPortfoliosMessage = 'No Further Content Available';
              this.activeContentType.nativeElement.innerHTML = 'Featured';
            }
            else {
              this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
              this.bsModalRef.content.message = 'Something went wrong.';
              this.showBackBtn = false;
              this.activeContentType.nativeElement.innerHTML = 'Featured';
            }
          }
        },
        (err) => {
          this.cookieService.delete('_token', '/');
          this.cookieService.delete('laravel_session', '/');
          this.cookieService.delete('XSRF-TOKEN', '/');
          this.getPortfolioProfiles(offset);
          this.showBackBtn = true;
        }
      );
    } else {
      this.uploadService.getPublicPortfolioProfiles(offset).subscribe(
        (res) => {
          if( res['status'] && (res['payload']['total'] > 0) ) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });
            this.showBackBtn = true;
            this.activeContentType.nativeElement.innerHTML = 'Featured';
          }
          else {
            if((res['payload']['total'] == 0)) {
              this.showBackBtn = true;
              // this.nextPortfoliosMessage = 'No Further Content Available';
              this.activeContentType.nativeElement.innerHTML = 'Featured';
            }
            else {
              this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
              this.bsModalRef.content.message = 'Something went wrong.';
              this.showBackBtn = true;
              this.activeContentType.nativeElement.innerHTML = 'Featured';
            }
          }
        },
        (err) => {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong.';
          this.showBackBtn = true;
        }
      );
    }
  }

  // Search 
  @ViewChild('searchMobInputWrapper') searchMobInputWrapper: ElementRef;
  @ViewChild('searchMobInput') searchMobInput: ElementRef;

  searchMobClick() {
    if(this.searchMobInputWrapper.nativeElement.classList.contains('displayNone')) {
      this.ren.removeClass(this.searchMobInputWrapper.nativeElement, 'displayNone');
    } else {
      this.ren.addClass(this.searchMobInputWrapper.nativeElement, 'displayNone');

      this.searchMobInput.nativeElement.focus();
    }
  }

  showDetailsForMobile(detailsWrapper: any, cardWrapperDoubleTap: any) {
    this.ren.addClass(detailsWrapper, 'openDetailsCard');
    this.ren.setStyle(cardWrapperDoubleTap, 'height', '0%');
  }

  hideDetailsForMobile(detailsWrapper: any, cardWrapperDoubleTap: any) {
    this.ren.removeClass(detailsWrapper, 'openDetailsCard');
    this.ren.setStyle(cardWrapperDoubleTap, 'height', '');
  }


  timer;
  touchduration = 500; // length of time we want the user to touch before we do something

  touchstart(detailsWrapper: any, cardWrapperDoubleTap: any) {
    this.timer = setTimeout( () => {
    this.showDetailsForMobile(detailsWrapper, cardWrapperDoubleTap);
    }, this.touchduration);
  }

  touchend(detailsWrapper: any, cardWrapperDoubleTap: any) {
    if (this.timer) {
      clearTimeout(this.timer); // clearTimeout, not cleartimeout..
      // this.hideDetailsForMobile(detailsWrapper);
    }
  }

  mouseEnterOncard(detailsWrapper: any, cardWrapperDoubleTap: any) {
    if(window.innerWidth >= 425) {
      this.showDetailsForMobile(detailsWrapper, cardWrapperDoubleTap);
    }
  }

  mouseLeaveOncard(detailsWrapper: any, cardWrapperDoubleTap: any) {
    if(window.innerWidth >= 425) {
      this.hideDetailsForMobile(detailsWrapper, cardWrapperDoubleTap);
    }
  }

  // Filter
  @ViewChild('sortMobInputWrapper') sortMobInputWrapper: ElementRef;
 
  sortMobClick() {
    if(this.sortMobInputWrapper.nativeElement.classList.contains('displayNone')) {
      this.ren.removeClass(this.sortMobInputWrapper.nativeElement, 'displayNone');
    } else {
      this.ren.addClass(this.sortMobInputWrapper.nativeElement, 'displayNone');
    }
  }
  
  @ViewChild('_contactPopupContainer', { read: ViewContainerRef })
  _contactPopupContainer: ViewContainerRef;

  contactPopupRef;
  contactPopupInstance;

  bsModalRef: BsModalRef;

  // Contact Modal
  showContactModal(modalType: number, portfolio_profile_id: number) {

    // Create Component
    let componentFactory = this.factoryResolver.resolveComponentFactory(ContactPopupComponent);
    this._contactPopupContainer.clear();
    this.contactPopupRef = this._contactPopupContainer.createComponent(componentFactory);
    this.contactPopupInstance = this.contactPopupRef.instance;

    // this.contactPopupInstance._sendOTP.subscribe(
    //   (value) => {
    //     this.uploadService.sendOTPForContact(value).subscribe(
    //       (res) => {
    //         if(res['type'] === 'success') {
    //           console.log("res : ", res);
    //         } else {
    //           this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
    //           this.bsModalRef.content.message = 'Invalid OTP';
    //         }
    //       },
    //       (err) => {
    //         this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
    //         this.bsModalRef.content.message = 'Invalid OTP';
    //       }
    //     );
    //   }
    // );

    this.contactPopupInstance.portfolio_profile_id = portfolio_profile_id;

    // Load ModalDirective late, as it is created dynamically
    setTimeout(() => {
      this.contactPopupInstance.showModal(modalType);
    },100);
  }

  contactHovered: boolean = false;

  contactHover() {
    // Desktop
    if(window.innerWidth > 768) {
      // $('.merge-btn-wrap').hover(function() {
      //   console.log('COMING TO MERGE BTN WRAP');
      //   $(this).find('.merge-btn-icon').addClass('mergeBtnIconHover');
      //   $(this).find('.merge-btn-label').addClass('mergeBtnLabelHover');
      // },
      // function() {
      //   $(this).find('.merge-btn-icon').removeClass('mergeBtnIconHover');
      //   $(this).find('.merge-btn-label').removeClass('mergeBtnLabelHover');
      // });
      this.contactHovered = !this.contactHovered;
    }
    // Mobile
    else {
      // $('.merge-btn-wrap').click(function() {
      //   $(this).find('.merge-btn-icon').addClass('mergeBtnIconHover');
      //   $(this).find('.merge-btn-label').addClass('mergeBtnLabelHover');
      // });
    }
  }

  clap(username: string, clapImg, clapCount) {
    console.log(this.isCookieSet);
    if(this.isCookieSet) {
      this.uploadService.clap(username).subscribe( (res) => {
        if(res['status'] === true) {
  
          if(res['payload']['clap'] === true) {
            // Change Logo to Royal Blue
            let filename = clapImg.getAttribute('src').split("/").slice(0, -1).join("/")+"/clap_royalblue.svg";
            this.ren.removeAttribute(clapImg, 'src');
            this.ren.setAttribute(clapImg, 'src', filename);
  
            // Change Views Count
            clapCount.innerHTML = parseInt(clapCount.innerHTML)+1;
          } else {
            // Change Logo to White
            let filename = clapImg.getAttribute('src').split("/").slice(0, -1).join("/")+"/clap_white.svg";
            this.ren.removeAttribute(clapImg, 'src');
            this.ren.setAttribute(clapImg, 'src', filename);
  
            // Change Views Count
            clapCount.innerHTML = parseInt(clapCount.innerHTML)-1;
          }
  
        } else {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = res['message'];
        }
      }, (err) => {
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Something went wrong while updating Content.';
      });
    } else {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'Please Login!';
      // $('#new-login-signup-modal').modal('show');
    }
  }

  @ViewChild('searchInput') searchInput: ElementRef;

  loadMorePortfolios() {
    this.scrolledNumber++;
    // Default
    if(this.listingType === 1) {
      this.getPortfolioProfiles(this.scrolledNumber);
    }
    // Searched
    else if(this.listingType === 2) {
      this.searchText(this.scrolledNumber);
    }
    // 
    else if(this.listingType == 3) {
      this.sortPortfolioProfiles(this.currentSort, this.scrolledNumber);
    }
  }

  searchText(offset: number) {

    this.listingType = 2;

    // Desktop
    if(window.innerWidth >= 425) {
      if(this.searchInput.nativeElement.value != '' && this.searchInput.nativeElement.value != undefined && this.searchInput.nativeElement.value != null) {
        this.uploadService.search(this.searchInput.nativeElement.value, offset).subscribe( (res) => {
          if( res['status'] && (res['payload']['total'] > 0) ) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });

            this.activeContentType.nativeElement.innerHTML = 'Searched For : '+this.searchInput.nativeElement.value;

            this.showBackBtn = true;
          }
          else {
            if((res['payload']['total'] == 0)) {
              this.nextPortfoliosMessage = 'No search results found.';
              this.activeContentType.nativeElement.innerHTML = 'Searched For : '+this.searchInput.nativeElement.value;
              this.showBackBtn = false;
            }
            else {
              this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
              this.bsModalRef.content.message = 'Something went wrong.';
              this.showBackBtn = false;
            }
          }

          this.searchMobClick();
        }, (err) => {
          this.showBackBtn = false;
        });
      } else {
        this.getPortfolioProfiles(1);
        this.activeContentType.nativeElement.innerHTML = 'Featured';
      }
    }
    // Mobile
    else {
      if(this.searchMobInput.nativeElement.value != '' && this.searchMobInput.nativeElement.value != undefined && this.searchMobInput.nativeElement.value != null) {
        this.uploadService.search(this.searchMobInput.nativeElement.value, offset).subscribe( (res) => {
          if( res['status'] && (res['payload']['total'] > 0) ) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });

            this.activeContentType.nativeElement.innerHTML = 'Searched For : '+this.searchMobInput.nativeElement.value;
            this.showBackBtn = true;
          }
          else {
            if((res['payload']['total'] == 0)) {
              this.nextPortfoliosMessage = 'No search results found.';
              this.activeContentType.nativeElement.innerHTML = 'Searched For : '+this.searchMobInput.nativeElement.value;
            }
            else {
              this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
              this.bsModalRef.content.message = 'Something went wrong.';
            }
            this.showBackBtn = false;
          }

          this.searchMobClick();
        }, (err) => {
          this.showBackBtn = false;
        });
      } else {
        this.getPortfolioProfiles(1);
        this.activeContentType.nativeElement.innerHTML = 'Featured';
      }
    }
  }

  @ViewChild('backToHome') backToHome: ElementRef;

  sortPortfolioProfiles(currentSort: number, offset: number) {

    this.listingType = 3;
    this.currentSort = currentSort;
    let self = this;

    if(this.currentSort == 1) {
      this.getPortfolioProfiles(1);
      this.activeContentType.nativeElement.innerHTML = 'Featured';
      if(window.innerWidth < 425) {
        this.sortMobClick();
      }
    } else if(this.currentSort == 2) {
      this.uploadService.sortedPortfolioProfiles('claps', offset).subscribe(
        (res) => {
          if(res['status'] === true) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });
            this.activeContentType.nativeElement.innerHTML = 'Sort by: most claps';
            this.showBackBtn = true;
          } else {
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong.';
            this.showBackBtn = false;
          }
          
          if(window.innerWidth < 425) {
            this.sortMobClick();
          }
        }, (err) => {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong.';
          this.showBackBtn = false;
        }
      );
    } else if(this.currentSort == 3) {
      this.uploadService.sortedPortfolioProfiles('views', offset).subscribe(
        (res) => {
          if(res['status'] === true) {
            res['payload']['data'].forEach((item, index) => {
              this.rowData.push(item);
            });
            this.activeContentType.nativeElement.innerHTML = 'Sort by: most views';
            this.showBackBtn = true;
          } else {
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong.';
            this.showBackBtn = false;
          }

          if(window.innerWidth < 425) {
            this.sortMobClick();
          }
        }, (err) => {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong.';
          this.showBackBtn = false;
        }
      );
    }
  }

  @ViewChild('activeContentType') activeContentType: ElementRef;

  searchEntered(event) {
    if (event.keyCode == 13) {
      this.rowData = [];
      this.searchText(1);
    }
  }

  changeInnerWidth(event) {
    this.windowInnerWidth = event.currentTarget.innerWidth;
  }

  @ViewChild('emailError') emailError: ElementRef;

  login(loginForm) {
    this.uploadService.login(loginForm.value.email, loginForm.value.password).subscribe( (res) => {
      if(res['status'] === true) {
        // console.log('TOKEN : ', res['payload']['_token']);
        this.cookieService.set('_token', res['payload']['_token']+"", parseInt(res['payload']['validity']), '/' );
        this.isCookieSet = true;
      } else {
        console.log('TOKEN : ', res['message']);
        this.emailError.nativeElement.innerHTML = res['message'];
      }
      $('#new-login-signup-modal').modal('hide');
    }, (err) => {
      $('#new-login-signup-modal').modal('hide');
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'Something went wrong while Login.';
    });
  }

  logout() {
    this.uploadService.logout().subscribe( (res) => {
      if(res['status'] == true) {
        this.cookieService.delete('_token', '/');
        this.cookieService.delete('laravel_session', '/');
        this.cookieService.delete('XSRF-TOKEN', '/');
        // this.r.navigate(['/']);
        this.isCookieSet = false;
      }
    }, (err) => {
      this.cookieService.delete('_token', '/');
      this.cookieService.delete('laravel_session', '/');
      this.cookieService.delete('XSRF-TOKEN', '/');
      document.location.href = "https://acadnion.com";
    });
  }
}
