import {  Component,
          OnInit,
          Input,
          Output,
          ElementRef,
          ViewChild,
          Renderer2,
          ContentChildren,
          EventEmitter,
          AfterViewInit,
          ChangeDetectionStrategy,
          ChangeDetectorRef,
          OnChanges,
          SimpleChanges
} from '@angular/core';

import Gifffer from 'gifffer';

import { VgMedia } from 'videogular2/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.directive';

import { EmbedVideoService } from 'ngx-embed-video';

declare var window: Window;

@Component({
  selector: 'view-grid',
  templateUrl: './view-grid.component.html',
  styleUrls: ['./view-grid.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewGridComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() show: string;
  @Input() asset: string;
  @Input() repositionImage: string;
  @Input() thumbnailImage: string;
  @Input() name: string;
  @Input() indexNumber: string;
  @Input() width: string;
  @Input() height: string;
  @Input() type: number;
  @Input('eachGridEl') eachGridEl: any;

  _typography: string;
  @Input('typography')
  set typography(value: string) {
  this._typography = value.split("|").join(", ");
  }

  get typography(): string {
  return this._typography;
  }

  _softwares: string;
  @Input('softwares')
  set softwares(value: string) {
  this._softwares = value.split("|").join(", ");
  }

  get softwares(): string {
  return this._softwares;
  }

  _technologies: string;
  @Input('technologies')
  set technologies(value: string) {
  this._technologies = value.split("|").join(", ");
  }

  get technologies(): string {
  return this._technologies;
  }

  _color_schemes: string;
  @Input('colorSchemes')
  set color_schemes(value: string) {
  this._color_schemes = value.split("|").join(", ");
  }

  get color_schemes(): string {
  return this._color_schemes;
  }

  @Input('inspiration') inspiration: string;

  @Output('showContentDetailsCard') showContentDetailsCardEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output('showActualAsset') showActualAssetEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output('hideActualAsset') hideActualAssetEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output('showContentDetailsForMobile') showContentDetailsForMobileEmit: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('source') source: ElementRef;
  @ViewChild('eachVideoPlayer') eachVideoPlayer: ElementRef;
  // @ContentChildren('eachGridWrapper', {descendants: true}) eachGridWrapper;

  @ViewChild(VgMedia) vgMedia: VgMedia;

  isToggled: boolean = false;
  isShownForDesktop: boolean = false;

  constructor(private el: ElementRef,
              private ren: Renderer2,
              private embedService: EmbedVideoService,
              private cdr: ChangeDetectorRef) {
    if(window.innerWidth >= 425) {
      this.isShownForDesktop = true;
    }
     else {
      this.isShownForDesktop = false;
    }
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.type == 2) {
      console.log(changes.width.currentValue);
      this.iFrameHtml = this.embedService.embed(changes.asset.currentValue, { query: { portrait: 0, color: '333' }, attr: { width: changes.eachGridEl.currentValue.$element.clientWidth, height: changes.eachGridEl.currentValue.$element.clientHeight } });
    }
  }

  ngAfterViewInit() {
    this.startGifffer();
  }

  gridClicked() {
    // For Desktop
    if(window.innerWidth >= 425) {
      if(this.type == 3) {
        // Hide Video
        if(this.isToggled) {
          this.ren.removeClass(this.el.nativeElement.querySelector('.asset-wrapper'), 'displayNone');
          console.log(':::', this.el.nativeElement.querySelector('.asset-wrapper'));
          // console.log(':::', this.el.nativeElement.querySelector('.description-wrapper'));
          console.log(':::', this.el.nativeElement.querySelector('.video-player-wrapper'));
          // this.ren.removeClass(this.el.nativeElement.querySelector('.description-wrapper'), 'displayNone');
          this.ren.addClass(this.el.nativeElement.querySelector('.video-player-wrapper'), 'displayNone');
          this.vgMedia.pause();
        }
        // Show Video
        else {
          this.ren.addClass(this.el.nativeElement.querySelector('.asset-wrapper'), 'displayNone');
          // this.ren.addClass(this.el.nativeElement.querySelector('.description-wrapper'), 'displayNone');
          this.ren.removeClass(this.el.nativeElement.querySelector('.video-player-wrapper'), 'displayNone');
          this.vgMedia.play();
        }
        this.isToggled = !this.isToggled;
      }
    }
  }

  showContentDetailsCard(event) {
    if( this.isContentDetails() ) {
      let posX: number = 20;
      let posY:number = 30;
      // if(this.eachGridEl.x > 0) {
        posX += this.eachGridEl._positionX;
      // }

      // console.log(this.eachGridEl.$element.clientHeight);
      // if(this.eachGridEl.y > 0) {
        posY += this.eachGridEl._positionY;
      // }
      console.log('pos : ', posX, posY);
      this.showContentDetailsCardEmit.emit({index: this.indexNumber, posX: posX , posY: posY});
    }
  }

  isContentDetails() {

    if( (this.typography == undefined || this.typography == "") &&
        (this.softwares == undefined || this.softwares == "") &&
        (this.technologies == undefined || this.technologies == "") &&
        (this.color_schemes == undefined || this.color_schemes == "") &&
        (this.inspiration == undefined || this.inspiration == "" || this.inspiration == "<p></p>") ) {
      return false;
    }

    return true;
  }

  isContentDetailsAvailable() {
    if( this.isContentDetails() ) {
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-toggle');
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-placement');
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'title');
    } else {
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-toggle', 'tooltip');
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-placement', 'top');
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'title', 'No Content Available!');
    }
  }

  gifs;

  startGifffer() {

    this.gifs = Gifffer({
      playButtonStyles: {
        'width': '60px',
        'height': '60px',
        'border-radius': '150%',
        'border': '3px solid rgba(255, 255, 255, 0.5)',
        'position': 'absolute',
        'top': '50%',
        'left': '50%',
        'margin': '-30px 0 0 -30px',
        "font-size": "22px",
        "color": "#fff",
        "letter-spacing": "1px",
        "display": "flex",
        "-webkit-box-align": "center",
        "-ms-flex-align": "center",
        "align-items": "center",
        "-webkit-box-pack": "center",
        "-ms-flex-pack": "center",
        "justify-content": "center"
      },
      playButtonIconStyles: {}
    });
  }

  showActualAsset() {
    this.showActualAssetEmit.emit(this.indexNumber);
  }

  hideActualAsset() {
    this.hideActualAssetEmit.emit(this.indexNumber);
  }

  timer;
  touchduration = 500; // length of time we want the user to touch before we do something

  onlongtouch() {
    this.showActualAsset();
  };

  touchstart() {
      this.timer = setTimeout( () => {
        this.showActualAsset();
      }, this.touchduration);
  }

  touchend() {
    if (this.timer) {
      clearTimeout(this.timer); // clearTimeout, not cleartimeout..
      this.hideActualAsset();
    }
  }

  showContentDetailsForMobile() {
    if(this.type != 3) {
      this.showContentDetailsForMobileEmit.emit(this.indexNumber);
    }
  }

  showInformationContent(event) {
    console.log('COMING TO INFO');
  }

  // playPauseVideo() {
  //   if(this.isToggled) {
  //     this.ren.removeClass(this.el.nativeElement.querySelector('.asset-wrapper'), 'displayNone');
  //     // this.ren.removeClass(this.el.nativeElement.querySelector('.description-wrapper'), 'displayNone');
  //     this.ren.addClass(this.el.nativeElement.querySelector('.video-player-wrapper'), 'displayNone');
  //     this.vgMedia.pause();
  //   } else {
  //     this.ren.addClass(this.el.nativeElement.querySelector('.asset-wrapper'), 'displayNone');
  //     // this.ren.addClass(this.el.nativeElement.querySelector('.description-wrapper'), 'displayNone');
  //     this.ren.removeClass(this.el.nativeElement.querySelector('.video-player-wrapper'), 'displayNone');
  //     this.vgMedia.play();
  //   }
  //   this.isToggled = !this.isToggled;
  // }

  closeVideoBtn() {
    if(this.type == 3) {
      // Hide Video
      if(this.isToggled) {
        this.ren.removeClass(this.el.nativeElement.querySelector('.asset-wrapper'), 'displayNone');
        // this.ren.removeClass(this.el.nativeElement.querySelector('.description-wrapper'), 'displayNone');
        this.ren.addClass(this.el.nativeElement.querySelector('.video-player-wrapper'), 'displayNone');
        this.vgMedia.pause();
      }
      this.isToggled = !this.isToggled;
    }
  }

  showVideoContentDetailsForMobile(event) {
    this.showContentDetailsForMobileEmit.emit(this.indexNumber);
  }

  iFrameHtml;
}
