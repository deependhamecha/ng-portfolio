import { Injectable } from '@angular/core';

import {  HttpEvent,
          HttpInterceptor,
          HttpHandler,
          HttpRequest
        }
        from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private cookieService: CookieService) { }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let token = (this.cookieService.get('_token') != null && this.cookieService.get('_token') != undefined) ? this.cookieService.get('_token') : 'tokennotset';

    let authReq = req.clone({
      headers: req.headers.set('Authorization', 'Bearer '+token)
    });

    return next.handle(authReq);
  }
}
