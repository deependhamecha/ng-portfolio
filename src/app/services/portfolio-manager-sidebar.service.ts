import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PortfolioManagerSidebarService {

  constructor() { }

  private fileChooserGridData = new Subject<any>();

  private fileChooserType = new Subject<any>();

  private webcamContent = new Subject<any>();

  private videoUrlContent = new Subject<any>();

  // Based on the object, portfolio-manager decides which modal to open
  setFileChooserGridData(gridData: any) {
    this.fileChooserGridData.next(gridData);
  }

  getFileChooserGridData(): Observable<any> {
    return this.fileChooserGridData.asObservable();
  }

  setFileChooserType(file) {
    this.fileChooserType.next(file);
  }

  getFileChooserType() {
    return this.fileChooserType.asObservable();
  }

  setWebCamContent(data: any) {
    this.webcamContent.next(data);
  }

  getWebCamContent() : Observable<any> {
    return this.webcamContent.asObservable();
  }

  setVideoUrlContent(data: any) {
    this.videoUrlContent.next(data);
  }

  getVideoUrlContent() : Observable<any> {
    return this.videoUrlContent.asObservable();
  }
}


