import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType, HttpEvent } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { PortfolioGrid } from '../portfolio_grid.model';
import { Headers } from '@angular/http';

var authkey = '144559AUboK6CuvX58c2e27f';

// "https://control.msg91.com/api/sendotp.php?authkey=144559AUboK6CuvX58c2e27f&mobile=8879101173&message=Your%20OTP%20is%200808&otp=0808"

// var settings = {
//   "async": true,
//   "crossDomain": true,
//   "url": undefined,
//   "method": "POST",
//   "headers": {}
// }

// $.ajax(settings).done(function (response) {
//   console.log(response);
// });

@Injectable()
export class UploadService {

  constructor(private http: HttpClient) { }

  /**
  * Upload New Image, Video, Doc or Gif
  */
  uploadFile(obj) : any {
    let form = new FormData();

    form.append('name', obj.name);
    form.append('asset', obj.asset);

    if(obj.typography != undefined) {
      form.append('typography', obj.typography);
    }

    if(obj.softwares != undefined) {
      form.append('softwares', obj.softwares);
    }

    if(obj.technologies != undefined) {
      form.append('technologies', obj.technologies);
    }

    if(obj.color_schemes != undefined) {
      form.append('color_schemes', obj.color_schemes);
    }

    if(obj.inspiration != undefined) {
      form.append('inspiration', obj.inspiration);
    }

    if(obj.thumbnail != undefined) {
      form.append('thumbnail', obj.thumbnail);
    }

    if(obj.device != undefined) {
      form.append('device', obj.device);
    }

    if(obj.megapixel != undefined) {
      form.append('megapixel', obj.megapixel);
    }

    if(obj.aperture != undefined) {
      form.append('aperture', obj.aperture);
    }

    if(obj.sensor != undefined) {
      form.append('sensor', obj.sensor);
    }

    if(obj.resolution != undefined) {
      form.append('resolution', obj.resolution);
    }

    if(obj.iso != undefined) {
      form.append('iso', obj.iso);
    }

    if(obj.shutterspeed != undefined) {
      form.append('shutterspeed', obj.shutterspeed);
    }

    let req = new HttpRequest('POST', '/acdn-portfolios/upload-file', form, {
      headers : new HttpHeaders().append('Access-Control-Allow-Origin', '*'),
      reportProgress: true
    });

    return this.http.request(req);
  }

  updateGridParameters(data: Array<{id: number, x_cord: number, y_cord: number, width:number, height: number}>) {

    return this.http.post('/acdn-portfolios/updategridparams',
                          data,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Content-Type', 'application/json')
                          }
                        );
  }

  loadEditGrid() {
    return this.http.get('/acdn-portfolios/loadeditgrid',
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*').append('Content-Type', 'application/json').append('Accept', 'application/json')
                        });
  }

  loadViewGrid(username) {
    return this.http.get('/acdn-portfolios/loadviewgrid/'+username,
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*').append('Content-Type', 'application/json')
                        });
  }

  getGrid(id, repo: boolean) {
    let url = '/acdn-portfolios/get-grid/'+id;

    if(repo) {
      url+='?repo';
    }

    return this.http.get(url,
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                        });
  }

  deleteGrid(gridIndex: number) {
    return this.http.delete('/acdn-portfolios/deletegrid/'+gridIndex,
                            {
                              headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                            });
  }

  repositionImage(id: number, image: Blob) {

    let form = new FormData();
    form.append('image', image);

    return this.http.post('/acdn-portfolios/reposition-image/'+id,
                        form,
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                        });
  }

  // Youtube and Vimeo URL Links
  getThumbnailFromURL(url: string) {
    let form = new FormData();
    form.append('url', url);

    return this.http.post('/acdn-portfolios/get-thumbnail-from-url',
                          form,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                          });
  }

  updateDescription(id: number, desc: string) {
    let form = new FormData();
    form.append('desc', desc);

    return this.http.post('/acdn-portfolios/update-description/'+id,
                          form,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                          });
  }

  uploadThumbnailImage(id: number, desc: string, file) {
    let form = new FormData();
    form.append('desc', desc);
    form.append('thumbnail', file);

    return this.http.post('/acdn-portfolios/upload-thumbnail/'+id,
                          form,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                          });
  }


  updateThumbnailImage(obj) {

    let form = new FormData();
    form.append('name', obj.name);

    if(obj.thumbnail != undefined) {
      form.append('thumbnail', obj.thumbnail);
    }

    if(obj.typography != undefined) {
      form.append('typography', obj.typography);
    }

    if(obj.softwares != undefined) {
      form.append('softwares', obj.softwares);
    }

    if(obj.technologies != undefined) {
      form.append('technologies', obj.technologies);
    }

    if(obj.color_schemes != undefined) {
      form.append('color_schemes', obj.color_schemes);
    }

    if(obj.inspiration != undefined) {
      form.append('inspiration', obj.inspiration);
    }

    if(obj.device != undefined) {
      form.append('device', obj.device);
    }

    if(obj.megapixel != undefined) {
      form.append('megapixel', obj.megapixel);
    }

    if(obj.aperture != undefined) {
      form.append('aperture', obj.aperture);
    }

    if(obj.sensor != undefined) {
      form.append('sensor', obj.sensor);
    }

    if(obj.resolution != undefined) {
      form.append('resolution', obj.resolution);
    }

    if(obj.iso != undefined) {
      form.append('iso', obj.iso);
    }

    if(obj.shutterspeed != undefined) {
      form.append('shutterspeed', obj.shutterspeed);
    }

    return this.http.post('/acdn-portfolios/update-thumbnail/'+obj.id,
                          form,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                          });

  }

  checkVideoUrl(url: string) {
    let form = new FormData();
    form.append('url', url);

    return this.http.post('/acdn-portfolios/check-video-url',
                          form,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                          });
  }

  uploadVideoUrl(obj) : any {

    let form = new FormData();

    form.append('name', obj.name);
    form.append('asset', obj.asset);

    if(obj.typography != undefined) {
      form.append('typography', obj.typography);
    }

    if(obj.softwares != undefined) {
      form.append('softwares', obj.softwares);
    }

    if(obj.technologies != undefined) {
      form.append('technologies', obj.technologies);
    }

    if(obj.color_schemes != undefined) {
      form.append('color_schemes', obj.color_schemes);
    }

    if(obj.inspiration != undefined) {
      form.append('inspiration', obj.inspiration);
    }

    if(obj.thumbnail != undefined) {
      form.append('thumbnail', obj.thumbnail);
    }

    let req = new HttpRequest('POST', '/acdn-portfolios/upload-video-url', form, {
      headers : new HttpHeaders().append('Access-Control-Allow-Origin', '*'),
      reportProgress: true
    });

    return this.http.request(req);
  }

  recaptcha(key: string): any {

    let data = {};
    data['key'] = key;

    return this.http.post('/acdn-portfolios/recaptcha',
                          data,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Content-Type', 'application/json')
                          }
                        );
  }

  /**
   *
   * @param token
   * @param provider
   *
   * Check is User Logged In/Out using token (Validate Token)
   * Do not send => Content-Type: application/json
   */
  checkAuthStatus(token: string, provider: string): any {

    let data = {};
    data['provider']=provider;

    // return this.http.post('/api/v/info',
    return this.http.get('/api/v/portfolio/info',
                          // data,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Accept', 'application/json')
                                                      .append('Authorization', 'Bearer '+token)
                          });
  }

  /**
  * Get Profile Info
  */
  getProfileInfo(username): any {

    let data = {};

    return this.http.get('/api/v/portfolio/viewinfo/'+username,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Accept', 'application/json')
                          });
  }

  /**
   * Public Get Portfolio Listing
   */
  getPublicPortfolioProfiles(offset: number): any {

    return this.http.get('/api/v/publicportfoliolisting?offset='+offset,
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                    .append('Accept', 'application/json')
                        });
  }

  /**
   * Get Portfolio Listing
   */
  getPortfolioProfiles(offset: number): any {

    return this.http.get('/api/v/portfoliolisting?offset='+offset,
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                    .append('Accept', 'application/json')
                        });
  }

  /**
   * Send SMS to API Service
   */
  sendOTPForContact(phoneNumber: string, portfolio_profile_id: number): any {

    let data = {};
    data['phone'] = phoneNumber;
    data['id'] = portfolio_profile_id;

    return this.http.post('/api/v/sendsmsforcontact',
                          data,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Accept', 'application/json')
                          });
  }

  /**
   * Validate OTP
   * @param otp 
   */
  validateOTPForContact(otp: number): any {

    let data = {};
    // return this.http.post('/api/v/sendsmsforcontact',
    //                       data,
    //                       {
    //                         headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
    //                                                   .append('Accept', 'application/json')
    //                       });
  }

  /**
  *   Clap
  */
 clap(username: string) {

   let data = {};
   data['username'] = username;
   data['provider'] = "student"; // To be removed in near future

   return this.http.post('/api/v/clap',
                          data,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Accept', 'application/json')
                          });

 }

 /**
 *  Search
 */
 search(search: string, offset: number) {

  return this.http.get('/api/v/portfolio/search?search='+search+'&offset='+offset,
                         {
                           headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                     .append('Accept', 'application/json')
                         });

 }

 logout() {
  return this.http.post('/api/v/logout',
                        {},
                        {
                          headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                    .append('Accept', 'application/json')
                        });
 }

  /**
   *  Sorted Portfolio Profiles
   * sort_by = claps/views
   */
  sortedPortfolioProfiles(sort_by: string, offset: number) {

    return this.http.get('/api/v/portfolio/sort?sort_by='+sort_by+'&offset='+offset,
    {
      headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                .append('Accept', 'application/json')
    });
  }

  /**
  *   Login from Modal
  */
  login(email: string, password: string) {

    // var formData = new FormData();
    // formData.append('email', email);
    // formData.append('password', password);

    var formData = {};
    formData['email'] = email;
    formData['password'] = password;

    return this.http.post('/acdn-portfolios/student/relogin',
                          formData,
                          {
                            headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                                      .append('Accept', 'application/json')
                          });

  }

  /**
  *   Clap Status for View and Edit Page
  */
  clapStatus(username: string) {
    return this.http.get('/api/v/clap-status/'+username,
    {
      headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
                                .append('Accept', 'application/json')
    });
  }
}
