import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';

import { CookieService } from 'ngx-cookie-service';
import { UploadService } from './upload.service';

@Injectable()
export class AuthService {

  /**
  * Made Subject so that, any component can access login/out status
  */
  // private loggedIn = new Subject<any>();
  loggedInOutStatus = false;

  constructor(private cookieService: CookieService,
              private uploadService: UploadService) {

    // console.log(">>>", this.cookieService.get('_token'));

    // // If Cookie is present, make log in status to true
    // if( this.cookieService.get('_token') != undefined &&
    //     this.cookieService.get('_token') != null &&
    //     this.cookieService.get('_token') != '') {
    //   // Validate Token with service
    //   this.uploadService
    //       .checkAuthStatus(this.cookieService.get('_token'), 'student')
    //       .subscribe( (success) => {
    //     this.loggedInOutStatus = true;
    //   }, (err) => {
    //     console.log('err : ', err);
    //   });
    // }
  }

  // setLogInOutStatus(loggedIn: boolean) {
  //   this.loggedIn.next(loggedIn);
  // }

  // getLogInOutStatus(): Observable<boolean> {
  //   return this.loggedIn.asObservable();
  // }


  // isAuthenticated() {
  //   let promise = new Promise(
  //     (resolve, reject) => {

  //       // If Cookie is present, make log in status to true
  //       if( this.cookieService.get('_token') !== undefined &&
  //           this.cookieService.get('_token') !== null &&
  //           this.cookieService.get('_token') !== '') {

  //         // Validate Token with service
  //         this
  //         .uploadService
  //         .checkAuthStatus(this.cookieService.get('_token'), 'student')
  //         .subscribe( (success) => {
  //           console.log("SUCCESS");
  //           this.loggedInOutStatus = true;
  //           resolve(this.loggedInOutStatus);
  //         }, (err) => {
  //           console.log('err : ', err);
  //           this.loggedInOutStatus = false;
  //           reject(this.loggedInOutStatus);
  //         });
  //       } else {
  //         this.loggedInOutStatus = false;
  //         reject(this.loggedInOutStatus);
  //       }
  //     }
  //   );

  //   return promise;
  // }

  isAuthenticated(): boolean {
     // If Cookie is present, make log in status to true
    if( this.cookieService.get('_token') !== undefined &&
        this.cookieService.get('_token') !== null &&
        this.cookieService.get('_token') !== '') {
      this.loggedInOutStatus = true;
      return this.loggedInOutStatus;
    } else {
      this.loggedInOutStatus = false;
      return this.loggedInOutStatus;
    }
  }

  /**
  * Check for Token
  * If _token cookie key is present then make loggedIn to true
  */
  logIn(): void {
    // if(this.cookieService.get('_token')) {

    // }
    this.loggedInOutStatus = true;
  }

  /**
  * Remove Token from cookie and redirect to digital portfolio route
  */
  logOut(): void {
    // Check Cookie and delete it from browser
    // if(this.cookieService.get('_token') != undefined || this.cookieService.get('_token') != null) {
    //   this.cookieService.delete('_token');
    // }

    this.loggedInOutStatus = false;
  }


}
