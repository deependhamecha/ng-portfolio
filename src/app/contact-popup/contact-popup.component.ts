import {  AfterViewInit,
          ViewChild,
          ElementRef,
          Input,
          Output,
          EventEmitter,
          Component,
          OnDestroy,
          ChangeDetectionStrategy,
          ChangeDetectorRef
} from '@angular/core';

import {  ModalDirective } from 'ngx-bootstrap';

import { UploadService } from '../services/upload.service';

import { Observable } from 'rxjs/Observable';

import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'contact-popup-modal',
  templateUrl: './contact-popup.component.html',
  styleUrls: ['./contact-popup.component.css']
})
export class ContactPopupComponent implements AfterViewInit, OnDestroy {

  @ViewChild('contactPopupModal')
  contactPopupModal: ModalDirective;

  portfolio_profile_id: number;

  // Typescript Sucks, keeping this as modalType variable doesnt recognise between instance and local variables!
  // Again Typescript Sucks
  modalTypeNumber: number = 0;

  smsSuccess: boolean = false;

  constructor(private el: ElementRef,
              private uploadService: UploadService) {}

  ngAfterViewInit() {
  }

  /**
   * 
   * @param modalType Phone=1, Mail=2, Chat=3
   */
  showModal(modalType: number) {
    setTimeout( () => {
      this.modalTypeNumber = modalType;
    }, 200);
    this.contactPopupModal.show();
  }

  hideModal() {
    this.contactPopupModal.hide();
  }

  reCaptchaSuccess = false;

  _recaptcha: Observable<any>;

  handleCorrectCaptcha(event) {

    this._recaptcha = this.uploadService.recaptcha(event).subscribe(
      res => {
        if(res['status']) {
          this.reCaptchaSuccess = true;
        } else {
          this.reCaptchaSuccess = false;
        }
      },
      err => {
        this.reCaptchaSuccess = false;
      }
    );
  }

  @ViewChild('contactPopupForm') contactPopupForm : NgForm;
  @ViewChild('otp') otp: ElementRef;

  /**
  *   Send OTP to respective person who needs contact information of portfolio card person
  */
  sendOTP() {
    // let authkey = '144559AUboK6CuvX58c2e27f';

    // let settings = {
    //   "async": true,
    //   "crossDomain": true,
    //   "url": 'http://control.msg91.com/api/sendotp.php?authkey='+authkey+'&message=Your%20OTP%20is%20'+randomNumber+'&mobile='+this.contactPopupForm['form'].value.phone+'&otp='+randomNumber,
    //   "method": "POST",
    //   "headers": {}
    // }
    
    // $.ajax(settings).done(function (response) {
    //   if(response['type'] === 'success') {
    //     this.otp.nativeElement.focusin();
    //   } else {
    //     this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
    //     this.bsModalRef.content.message = 'Invalid OTP';
    //   }
    // });

    this.uploadService.sendOTPForContact(this.contactPopupForm.value.phone, this.portfolio_profile_id).subscribe( (res) => {
      console.log('SUCCESS : ', res['payload']);
    }, (err) => {});
  }

  validateOTP(otp: number) {

    this.uploadService.validateOTPForContact(this.contactPopupForm.value.otp).subscribe( (res) => {
    }, (err) => {

    });
  }

  ngOnDestroy() {

  }

}