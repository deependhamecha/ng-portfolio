import { Component,
         OnInit,
         Input,
         Output,
         AfterViewInit,
         ViewChild,
         ElementRef,
         EventEmitter,
         OnChanges,
         SimpleChanges } from '@angular/core';

// JQuery
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.css']
})
export class RichTextEditorComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('txtEditor', {read: ElementRef}) txtEditor: ElementRef;

  @Input() value: string;

  @Output() valueChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let classThis = this;
    console.log('summernote : ', this.txtEditor.nativeElement);
    $(this.txtEditor.nativeElement).summernote({
      height: 80,
      toolbar: [
          ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']]
      ],
      callbacks: {
          onChange: function(contents, $editable) {
            classThis.valueChanged.emit({value: contents});
            this.value = contents;
          }
      }
    });

    $(this.txtEditor.nativeElement).summernote('code', this.value);

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("CHANGES : ",changes);
  }
}
