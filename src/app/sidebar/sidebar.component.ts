import {  Component,
          OnInit,
          Input,
          Output,
          EventEmitter,
          ViewChild,
          ElementRef,
          ViewContainerRef,
          ComponentFactoryResolver,
          OnDestroy
        } from '@angular/core';

import { PortfolioManagerSidebarService } from '../services/portfolio-manager-sidebar.service';


import { NgForm } from '@angular/forms';
import { NgProgress } from '@ngx-progressbar/core';

import { UploadService } from '../services/upload.service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WebCamModalComponent } from '../web-cam-modal/web-cam-modal.component';
import { VideoUrlModalComponent } from '../video-url-modal/video-url-modal.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  @Input('webCamModalContainer')
  webCamModalContainer: ViewContainerRef;

  @Input('videoUrlModalContainer')
  videoUrlModalContainer: ViewContainerRef;

  @Input('navToggleVal')
  navToggleVal: boolean;

  @Output('navToggle') navToggleEmit: EventEmitter<void> = new EventEmitter<void>();

  lastWindowWidth: number;
  isShownForDesktop: boolean;
  isContactMobileWrapperShown: boolean;

  constructor(private pmss: PortfolioManagerSidebarService,
              private progressService: NgProgress,
              private uploadService: UploadService,
              private el: ElementRef,
              private factoryResolver: ComponentFactoryResolver,
              private modalService: BsModalService) {
    this.lastWindowWidth = window.innerWidth;
    if(window.innerWidth >= 425) {
      this.isShownForDesktop = true;
      this.isContactMobileWrapperShown = false;
    } else {
      this.isShownForDesktop = false;
      this.isContactMobileWrapperShown = true;
    }
  }

  ngOnInit() {
  }

  bsModalRef: BsModalRef;

  navToggle() {
    this.navToggleEmit.emit();
  }

  setFileChooserType(file, no) {
    this.pmss.setFileChooserType({file: file, no: no});
    setTimeout(() => {
      this.el.nativeElement.querySelector('#imageFileChooser').value = "";
      this.el.nativeElement.querySelector('#videoFileChooser').value = "";
      this.el.nativeElement.querySelector('#docFileChooser').value = "";
      this.el.nativeElement.querySelector('#pdfFileChooser').value = "";
    }, 1000);
  }

  webCamComponentInstance;
  webCamComponentRef;

  videoUrlComponentInstance;
  videoUrlModalComponentRef;

  showWebCamModal() {
    // create Component
    let componentFactory = this.factoryResolver.resolveComponentFactory(WebCamModalComponent);
    this.webCamModalContainer.clear();
    this.webCamComponentRef = this.webCamModalContainer.createComponent(componentFactory);
    this.webCamComponentInstance = this.webCamComponentRef.instance;
    console.log('webCamComponentInstance : ', this.webCamComponentInstance);
    this.webCamComponentInstance = this.webCamComponentInstance.getImageEmit.subscribe(event => {
      this.pmss.setWebCamContent(event);
    });
  }

  showVideoUrlModal() {
    let self = this;
    let componentFactory = this.factoryResolver.resolveComponentFactory(VideoUrlModalComponent);
    this.videoUrlModalContainer.clear();
    this.videoUrlModalComponentRef = this.videoUrlModalContainer.createComponent(componentFactory);
    this.videoUrlComponentInstance = this.videoUrlModalComponentRef.instance;
    this.videoUrlComponentInstance = this.videoUrlComponentInstance.getDataEmit.subscribe(event => {
      this.pmss.setVideoUrlContent(event);
    });
  }

  ngOnDestroy() {
    if(this.webCamComponentInstance != undefined && this.webCamComponentInstance != null) {
      this.webCamComponentInstance.unsubscribe();
      this.webCamModalContainer.clear();
      this.webCamModalContainer.detach();
    }

    if(this.videoUrlComponentInstance != undefined && this.videoUrlComponentInstance != null) {
      this.videoUrlComponentInstance.unsubscribe();
      this.videoUrlModalContainer.clear();
      this.videoUrlModalContainer.detach();
    }
  }

  onWindowResize(event) {

    if(window.innerWidth >= 768 && this.lastWindowWidth < 768) {

    } else if(window.innerWidth < 768 && this.lastWindowWidth >= 768) {

    }
    this.lastWindowWidth = window.innerWidth;
  }
}
