import {  Component,
          OnInit,
          ViewChild,
          EventEmitter,
          Output,
          OnDestroy,
          AfterViewInit,
          ElementRef,
          Renderer2
} from '@angular/core';

import { NgForm } from '@angular/forms';

import { UploadService } from '../services/upload.service';
import { PortfolioManagerSidebarService } from '../services/portfolio-manager-sidebar.service';
import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';

import {  BsModalRef } from 'ngx-bootstrap';

import {  ModalDirective,
          BsModalService
} from 'ngx-bootstrap/modal';

import { NgProgress } from '@ngx-progressbar/core';

@Component({
  selector: 'app-video-url-modal',
  templateUrl: './video-url-modal.component.html',
  styleUrls: ['./video-url-modal.component.css']
})
export class VideoUrlModalComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output()
  getDataEmit: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('updateVideoUrlModal')
  updateVideoUrlModal: ModalDirective;

  bsModalRef : BsModalRef;
  uploadServiceInstance: any;

  isDisabled=true;
  @ViewChild('videoThumbnailMessage') videoThumbnailMessage: ElementRef;
  @ViewChild('videoUrlCheckIcon') videoUrlCheckIcon: ElementRef;
  @ViewChild('videoUrlCrossIcon') videoUrlCrossIcon: ElementRef;

  constructor(private progressService: NgProgress,
              private uploadService: UploadService,
              private pmss : PortfolioManagerSidebarService,
              private modalService: BsModalService,
              private ren: Renderer2) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.openUpdateVideoUrlModal();
  }

  hideVideoUrlModal() {
    this.updateVideoUrlModal.hide();
  }

  openUpdateVideoUrlModal() {
    this.updateVideoUrlModal.show();
  }

  checkVideoUrl(url) {

    this.uploadService.checkVideoUrl(url).subscribe(res => {
      if(res['status']) {
        this.ren.addClass(this.videoUrlCrossIcon.nativeElement, 'displayNone');
        this.ren.removeClass(this.videoUrlCheckIcon.nativeElement, 'displayNone');
        this.isDisabled = false;
      } else {
        this.ren.addClass(this.videoUrlCheckIcon.nativeElement, 'displayNone');
        this.ren.removeClass(this.videoUrlCrossIcon.nativeElement, 'displayNone');
        this.isDisabled = true;
      }
    },
    err => {
      this.ren.addClass(this.videoUrlCheckIcon.nativeElement, 'displayNone');
      this.ren.removeClass(this.videoUrlCrossIcon.nativeElement, 'displayNone');
      this.isDisabled = true;
    });
  }

  updateVideoUrlSubmit(form: NgForm) {
    this.progressService.start();

    this.getDataEmit.emit(form.value.videoUrl);
  }

  ngOnDestroy() {
    if(this.uploadServiceInstance != undefined) {
      this.uploadServiceInstance.unsubscribe();
    }
  }
}
