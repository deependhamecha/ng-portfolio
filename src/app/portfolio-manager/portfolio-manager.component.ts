import {
  Component,
  ViewChild,
  AfterViewInit,
  ElementRef,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  ViewContainerRef,
  OnChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  AfterViewChecked,
  ComponentRef,
  TemplateRef,
  Inject
} from '@angular/core';

import { FileDropModule, UploadEvent, UploadFile } from 'ngx-file-drop';

import { HttpClient, HttpEventType, HttpEvent } from '@angular/common/http';

// Bootstrap
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { UploadService } from '../services/upload.service';

import { NgProgressModule, NgProgress } from '@ngx-progressbar/core';

import { GridsterComponent, GridsterItemComponent, GridsterItemPrototypeDirective } from 'angular2gridster';

import { CommonService } from '../services/common.service';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { NgForm } from '@angular/forms';

import Croppie from 'croppie';
import { PortfolioManagerSidebarService } from '../services/portfolio-manager-sidebar.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';

import { Select2OptionData } from 'ng2-select2';

// import { PortfolioGrid } from '../portfolio_grid.interface';
import { FormGroup, FormControl } from '@angular/forms';
import { FormArray } from '@angular/forms/src/model';

import { PortfolioGrid } from '../portfolio_grid.model';
import { TabsetComponent } from 'ngx-bootstrap';
import { WebCamComponent } from 'ack-angular-webcam';
import { WebCamModalComponent } from '../web-cam-modal/web-cam-modal.component';
import { VideoUrlModalComponent } from '../video-url-modal/video-url-modal.component';
import { GridComponent } from '../grid/grid.component';
import { ViewChildren } from '@angular/core';
import { setTimeout } from 'core-js/library/web/timers';

import { IGridsterOptions } from 'angular2gridster';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

// JQuery
declare var jquery: any;
declare var $: any;
declare var window: Window;
declare var document: Document;

@Component({
  selector: 'portfolio-manager',
  templateUrl: './portfolio-manager.component.html',
  styleUrls: ['./portfolio-manager.component.css']
})
export class PortfolioManagerComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy, OnChanges {

  // @Input() gridElements: Array<any>;
  // @Output('saveText') saveText = new EventEmitter<string>();

  @Output('navToggle') navToggle: EventEmitter<any> = new EventEmitter<any>();

  bsModalRef: BsModalRef;

  // public files: UploadFile[] = [];
  private lastDroppedEvent = null;

  // Temporary Save State of the User for Sending data to server
  tempGridData: Array<{ id: number, x_cord: number, y_cord: number, width: number, height: number }> = new Array();

  isRequestSent: boolean = false;

  @ViewChild(GridsterComponent) gridster: GridsterComponent;
  // @ViewChild(GridsterItemPrototypeDirective) gic : GridsterItemPrototypeDirective;
  @ViewChild('editContentTabs') editContentTabs: TabsetComponent
  @ViewChild('imageUploadContentTabs') imageUploadContentTabs: TabsetComponent;
  @ViewChild('contentUploadTabs') contentUploadTabs: TabsetComponent;
  @ViewChild('videoUrlUploadContentTabs') videoUrlUploadContentTabs: TabsetComponent;

  //--------------------------------Reposition Modal Variables----------------------------------------
  @ViewChild('repositionImage') repositionImage: ElementRef;

  croppieInstance: Croppie;

  @ViewChild('updateRepositionImageModal') updateRepositionImageModal: ModalDirective;

  tmpData = {};
  portfolioGrid: PortfolioGrid = new PortfolioGrid();

  //--------------------------------------------------------------------------------------------------
  supportedImageFormats = ['jpg', 'jpeg', 'bmp', 'png', 'svg'];
  supportedVideoFormats = ['mkv', 'avi', 'mp4', 'mpg', 'mpeg'];
  supportedDocFormats = ['doc', 'docx', 'odt', 'rtf', 'pdf', 'xml', 'json'];
  supportedGifFormats = ['gif'];

  fileChooserGridData: Subscription;
  fileChooserType: Subscription;
  webCamContent: Subscription;
  videoUrlContent: Subscription;

  currentSelectedActionForMobile: any;

  @ViewChild('imageModalThumbnail') imageModalThumbnail: HTMLInputElement;
  // @ViewChild('uploadVideoThumbnailModalThumbnail') uploadVideoThumbnailModalThumbnail: HTMLInputElement;
  // @ViewChild('uploadDocThumbnailModalThumbnail') uploadDocThumbnailModalThumbnail: HTMLInputElement;

  @Input() webCamModalContainer: ViewContainerRef;
  @ViewChild(WebCamModalComponent) webCamComponent: ComponentRef<any>;

  @Input() videoUrlModalContainer: ViewContainerRef;
  @ViewChild(VideoUrlModalComponent) videoUrlComponent: ComponentRef<any>;

  isCookieSet = false;

  _paramsSub;

  routeUsername: string = '';

  constructor(private uploadService: UploadService,
              private modalService: BsModalService,
              public progressService: NgProgress,
              private el: ElementRef,
              private pmss: PortfolioManagerSidebarService,
              private ren: Renderer2,
              private cdr: ChangeDetectorRef,
              private r: Router,
              private route: ActivatedRoute,
              private cookieService: CookieService) {

    this._paramsSub = this.route.params.subscribe( (param) => {
      this.routeUsername = param['username'];
    });
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  message: string;

  gridboxes: Array<any>;
  gridboxWidth: number;
  gridboxHeight: number;

  @ViewChild('imageUploadForm', { read: NgForm }) imageUploadForm: NgForm;
  @ViewChild('videoUrlForm', { read: NgForm }) videoUrlForm: NgForm;

  ngAfterViewInit() {

    // Check Login Token
    if( this.cookieService.get('_token') !== undefined &&
        this.cookieService.get('_token') !== null &&
        this.cookieService.get('_token') !== '') {

      this.uploadService.checkAuthStatus(this.cookieService.get('_token'), 'student').subscribe((res) => {
        this.isCookieSet = true;

        // Is Request Username Profile === LoggedIn Username
        if(this.routeUsername != '' && res['payload']['username'] == this.routeUsername) {
          this.loadGrid();
        } else {
          this.r.navigate(['/']);
        }

        this.fileChooserGridData = this.pmss.getFileChooserGridData().subscribe(fileChooserGridData => {
          if(window.innerWidth < 425) {
            this.navToggle.emit();
          }
          this.addEl(fileChooserGridData['payload']);
        });
    
        this.fileChooserType = this.pmss.getFileChooserType().subscribe(fileChooserType => {
          this.portfolioGrid.reset();
          this.portfolioGrid.asset = fileChooserType.file;
          if(window.innerWidth < 425) {
            this.navToggle.emit();
          }
          this.showRespectiveModal(fileChooserType.no);
        });
    
        this.webCamContent = this.pmss.getWebCamContent().subscribe(data => {
          if (data != undefined) {
            this.portfolioGrid.reset();
            this.portfolioGrid.asset = data['file'];
            this.webCamModalContainer.clear();
            this.webCamModalContainer.detach();
            if(window.innerWidth < 425) {
              this.navToggle.emit();
            }
            this.showRespectiveModal(1);
          } else {
            // Error Condition
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong while updating Content.';
          }
        });
    
        let self = this;
        this.videoUrlContent = this.pmss.getVideoUrlContent().subscribe(data => {
          if (data != undefined) {
            this.portfolioGrid.reset();
            this.portfolioGrid.asset = data;
            this.portfolioGrid.type = 3;
            this.videoUrlModalContainer.clear();
            this.videoUrlModalContainer.detach();
            if(window.innerWidth < 425) {
              this.navToggle.emit();
            }
            this.showRespectiveModal(2);
          } else {
            // Error Condition
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong while updating Content.';
          }
        });

        if(window.innerWidth < 425) {
          if(document.body.clientWidth < window.innerWidth) {
            alert('Go to Settings > Accessibility > Turn off Force Scaling');
            // window.location.href=window.location.href;
          }
        }
        
        this.imageUploadForm.valueChanges.subscribe(data => {
          this.portfolioGrid.name = data.name;
          this.portfolioGrid.setDevice(data.device);
          this.portfolioGrid.setMegapixel(data.megapixel);
          this.portfolioGrid.setAperture(data.aperture);
          this.portfolioGrid.setSensor(data.sensor);
          this.portfolioGrid.setResolution(data.resolution);
          this.portfolioGrid.setIso(data.iso);
          this.portfolioGrid.setShutterspeed(data.shutterspeed);
        });
    
        this.uploadVideoThumbnailModalForm.valueChanges.subscribe(data => {
          this.portfolioGrid.name = data.name;
          this.portfolioGrid.setDevice(data.device);
          this.portfolioGrid.setMegapixel(data.megapixel);
          this.portfolioGrid.setAperture(data.aperture);
          this.portfolioGrid.setSensor(data.sensor);
          this.portfolioGrid.setResolution(data.resolution);
          this.portfolioGrid.setIso(data.iso);
          this.portfolioGrid.setShutterspeed(data.shutterspeed);
        });
    
        this.uploadDocThumbnailModalForm.valueChanges.subscribe(data => {
          this.portfolioGrid.name = data.name;
          this.portfolioGrid.setDevice(data.device);
          this.portfolioGrid.setMegapixel(data.megapixel);
          this.portfolioGrid.setAperture(data.aperture);
          this.portfolioGrid.setSensor(data.sensor);
          this.portfolioGrid.setResolution(data.resolution);
          this.portfolioGrid.setIso(data.iso);
          this.portfolioGrid.setShutterspeed(data.shutterspeed);
        });
    
        this.videoUrlForm.valueChanges.subscribe(data => {
          this.portfolioGrid.name = data.name;
          this.portfolioGrid.setDevice(data.device);
          this.portfolioGrid.setMegapixel(data.megapixel);
          this.portfolioGrid.setAperture(data.aperture);
          this.portfolioGrid.setSensor(data.sensor);
          this.portfolioGrid.setResolution(data.resolution);
          this.portfolioGrid.setIso(data.iso);
          this.portfolioGrid.setShutterspeed(data.shutterspeed);
        });
    
        this.tmpData['showEditContentModal'] = false;
    
        this.editContentModalForm.valueChanges.subscribe(data => {
          this.portfolioGrid.name = data.name;
          this.portfolioGrid.setDevice(data.device);
          this.portfolioGrid.setMegapixel(data.megapixel);
          this.portfolioGrid.setAperture(data.aperture);
          this.portfolioGrid.setSensor(data.sensor);
          this.portfolioGrid.setResolution(data.resolution);
          this.portfolioGrid.setIso(data.iso);
          this.portfolioGrid.setShutterspeed(data.shutterspeed);
        });
      }, (err) => {
        this.isCookieSet = false;
        this.cookieService.delete('_token', '/');
        this.cookieService.delete('larave_session', '/');
        this.cookieService.delete('XSRF-TOKEN', '/');
        
        this.r.navigate(['/errorpage/404']);
      });
    } else {
      this.isCookieSet = false;
      this.cookieService.delete('_token', '/');
      this.cookieService.delete('larave_session', '/');
      this.cookieService.delete('XSRF-TOKEN', '/');
      this.r.navigate(['/errorpage/404']);
    }

  }

  toggleGridBorder() {
    if ($('.grid-box-wrapper').hasClass('opa')) {
      $('.grid-box-wrapper').removeClass('opa');
    } else {
      $('.grid-box-wrapper').addClass('opa');
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("changes : ", changes);
  }

  widgets: Array<any> = [];

  gridsterOptions: IGridsterOptions = {
    lanes: 4, // how many lines (grid cells) dashboard has
    direction: 'vertical', // items floating direction: vertical/horizontal
    widthHeightRatio: 1,
    shrink: true,
    lines: {
      visible: true,
      color: '#444BD6',
      width: 2
    },
    responsiveView: true,
    resizable: true, // possible to resize items by drag n drop by item edge/corner
    dragAndDrop: true, // possible to change items position by drag n drop
    useCSSTransforms: true,
    floating: true
  };

  // Add Element to Grid
  addEl(data) {

    let el = {};

    if (data == undefined || Object.keys(data).length < 1) {
      console.log('No Data Found');
    } else {

      if (data.asset_from == "reposition") {
        data.asset = data.reposition_image;
      } else if (data.asset_from == "thumbnail") {
        data.asset = data.thumbnail_image;
      } else if (data.asset_from == "asset") {
        data.asset = data.asset;
      } else {
        data.asset = "nothing";
      }

      el = {
        x: data.x_cord,
        y: data.y_cord,
        w: data.width,
        h: data.height,
        dragAndDrop: true,
        resizable: true,
        id: data.id,
        name: data.name,
        asset: data.asset,
        type: data.type,
        typography: data.typography,
        softwares: data.softwares,
        technologies: data.technologies,
        color_schemes: data.color_schemes,
        inspiration: data.inspiration
      };

      this.widgets.push(el);
      return (this.widgets.length - 1);
    }
  }

  changedX(el, event: Event) {
    console.log('CHANGED X START');
    console.log(event);
    console.log('CHANGED X END')
  }

  changedY(el, event: Event) {
    console.log('CHANGED Y START');
    console.log(event);
    console.log('CHANGED Y END');
  }

  changedW(el, event: Event) {
    console.log('CHANGED W');
    console.log(event);
  }

  changedH(el, event: Event) {
    console.log('CHANGED H');
    console.log(event);
  }

  // Drag and Drop Zone Listeners

  fileOver(event) {
    this.el.nativeElement.querySelector('#dropZone').style.background = '#fff';
    // this.el.nativeElement.querySelector('#dropZone>.content').style.color = '#fff';
    this.el.nativeElement.querySelector('#dropZone>.content>img').style.filter = 'blur(5px)';
  }

  fileLeave(event) {
    this.el.nativeElement.querySelector('#dropZone').style.background = '#fff';
    // this.el.nativeElement.querySelector('#dropZone').querySelector('.content').style.color = '#444BD6';
    this.el.nativeElement.querySelector('#dropZone>.content>img').style.filter = '';
  }

  @ViewChild('imageModalForm') imageModalForm: NgForm;

  dropped(event: UploadEvent) {
    this.el.nativeElement.querySelector('#dropZone').style.background = '#fff';
    let extension = event.files[0].fileEntry.name.split('.').pop();

    this.portfolioGrid.reset();

    event.files[0].fileEntry.file((info) => {
      this.portfolioGrid.asset = info;
    });

    if (this.supportedImageFormats.includes(extension)) {
      this.showRespectiveModal(1);
    } else if (this.supportedVideoFormats.includes(extension)) {
      this.showRespectiveModal(3);
    } else if(this.supportedDocFormats.includes(extension)) {
      this.showRespectiveModal(4);
    } else if(this.supportedGifFormats.includes(extension)) {
      this.showRespectiveModal(1);
    } else {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'File Type not recognized.';
    }

  }

  // @ViewChild('imageDescText') imageDescText: ElementRef;
  @ViewChild('uploadVideoThumbnailModalForm') uploadVideoThumbnailModalForm: NgForm;
  @ViewChild('uploadDocThumbnailModalForm') uploadDocThumbnailModalForm: NgForm;
  // @ViewChild('uploadVideoThumbnailModalDescText') uploadVideoThumbnailModalDescText: ElementRef;
  // @ViewChild('uploadDocThumbnailModalDescText') uploadDocThumbnailModalDescText: ElementRef;

  // 1 for Image Modal, 3 for Video, 4 for Doc/PDF
  showRespectiveModal(no: number) {
    if (no == 1) {
      this.imageUploadForm.value.name = '';
      this.imageUploadForm.value.typography = '';
      this.portfolioGrid.typography = '';
      this.imageUploadForm.value.softwares = '';
      this.imageUploadForm.value.technologies = '';
      this.imageUploadForm.value.color_schemes = '';
      this.imageUploadForm.value.inspiration = '';
      this.imageUploadForm.value.id = undefined;
      this.imageUploadContentTabs.tabs[0].active = true;
      this.imageModal.show();
    }
    else if (no == 2) {
      this.videoUrlUploadContentTabs.tabs[0].active = true;
      this.videoUrlContentModal.show();
    }
    else if (no == 3) {
      this.uploadVideoThumbnailModalForm.value.name = '';
      this.uploadVideoThumbnailModalForm.value.thumbnail = '';
      this.uploadVideoThumbnailModalForm.value.typography = '';
      this.uploadVideoThumbnailModalForm.value.softwares = '';
      this.uploadVideoThumbnailModalForm.value.technologies = '';
      this.uploadVideoThumbnailModalForm.value.color_schemes = '';
      this.uploadVideoThumbnailModalForm.value.inspiration = '';
      this.uploadVideoThumbnailModalForm.value.id = undefined;

      this.uploadVideoThumbnailModal.show();
    }
    else if (no == 4) {
      this.uploadDocThumbnailModalForm.value.name = '';
      this.uploadDocThumbnailModalForm.value.thumbnail = '';
      this.uploadDocThumbnailModalForm.value.typography = '';
      this.uploadDocThumbnailModalForm.value.softwares = '';
      this.uploadDocThumbnailModalForm.value.technologies = '';
      this.uploadDocThumbnailModalForm.value.color_schemes = '';
      this.uploadDocThumbnailModalForm.value.inspiration = '';
      this.uploadDocThumbnailModalForm.value.id = undefined;

      this.uploadDocThumbnailModal.show();
    }
    else {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'Something went wrong while uploading Content.';
    }
  }

  @ViewChildren(GridComponent) allGridComponents: GridComponent;

  itemChanged(event, tr: ElementRef, index) {

    console.log('WIDGETS');
    console.log(this.widgets);
    console.log('****');
    console.log(event);
    console.log('****');

    let data = {
      id: parseInt(tr['$element'].id),
      x_cord: parseInt(tr['x']),
      y_cord: parseInt(tr['y']),
      width: parseInt(tr['w']),
      height: parseInt(tr['h'])
    };

    console.log(this.allGridComponents['_results'][index]);

    if (data.id != undefined) {

      // console.log(data.id);
      let index = this.pluck(this.tempGridData, 'id').indexOf(data.id);
      // console.log('~~'+index);
      if (index != -1) {
        // console.log('BEFORE:::');
        // console.log(this.tempf);
        this.tempGridData[index] = data;
        // console.log('AFTER:::');
        // console.log('fffff');
        // console.log(this.widgets);
        // console.log('fffff');
      } else {
        this.tempGridData.push(data);
      }
      /*  If this condition is true then it means there was no pending 10sec timeout request
      *   and it will send request after 10sec. (Dont Judge on variable's name!)
      */
      if (!this.isRequestSent) {
        this.progressService.start();
        this.isRequestSent = true;

        setTimeout(() => {

          this.uploadService.updateGridParameters(this.tempGridData).subscribe(
            (res) => {
              if (res['status']) {
                this.tempGridData = [];
                this.gridster.reload();

                if(window.innerWidth < 425) {
                  setTimeout( () => {
                    this.gridster
                        .setOption('dragAndDrop', this.isGridLocked)
                        .setOption('resizable', this.isGridLocked)
                        .setOption('direction', this.isGridLocked ? 'vertical': 'none')
                        .setOption('floating', this.isGridLocked)
                        .reload();
                  });
                }

                this.isRequestSent = false;
              } else {
                // Error Condition
                this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
                this.bsModalRef.content.message = 'Something went wrong while updating Content.';
              }

              this.progressService.done();
            },
            (err) => {
              // Error Condition
              this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
              this.bsModalRef.content.message = 'Something went wrong while updating Content.';

              this.tempGridData = [];
              // this.isSaved = false;
              this.isRequestSent = false;
            }
          );
        }, 100);

      }
    }
  }

  pluck = (arr, key) => {
    let newArr = [];
    for (var i = 0, x = arr.length; i < x; i++) {
      if (arr[i].hasOwnProperty(key)) {
        newArr.push(arr[i][key]);
      }
    }
    return newArr;
  }

  removeGrid(indexEl: HTMLInputElement) {

    let index = +indexEl.value;

    this.uploadService.deleteGrid(this.widgets[index]['id']).subscribe(
      res => {
        this.tmpData = {};
        if (res['status']) {
          this.widgets.splice(index, 1);
          this.hideConfirmDeleteModal();
        } else {
          this.hideConfirmDeleteModal();
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong while Deleting Content.';
        }
      },
      err => {
        this.tmpData = {};
        this.hideConfirmDeleteModal();
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Something went wrong while Deleting Content.';
      }
    );
  }

  @ViewChild('gridLoader') gridLoader: ElementRef;

  loadGrid() {
    this.uploadService.loadEditGrid().subscribe(
      (res) => {
        if (res['status']) {
          this.widgets = [];
          let gridElements = res['payload'];

          gridElements.forEach((element, i) => {
            this.addEl(element);
          });


          if(window.innerWidth < 425) {
            setTimeout( () => {
              this.gridster
                  .setOption('dragAndDrop', 'false')
                  .setOption('resizable', 'false')
                  .setOption('direction', 'none')
                  .reload();
            });
          }
        } else {
          // Redirect for Invalid Username
          this.r.navigate(['/errorpage/404']);

          // this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          // this.bsModalRef.content.message = 'Please refresh the page as there has been an error loading content.';
        }

        // Grid Loader Hide After Loading Grid Elements
        this.gridLoader.nativeElement.style.display = "none";
      },
      (err) => {
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Please refresh the page as there has been an error loading content.';
      }
    );
  }

  reloadGrid() {
    this.loadGrid();
  }

  @ViewChild('imageModal') public imageModal: ModalDirective;
  @ViewChild('uploadVideoThumbnailModal') public uploadVideoThumbnailModal: ModalDirective;
  @ViewChild('uploadDocThumbnailModal') public uploadDocThumbnailModal: ModalDirective;
  @ViewChild('editContentModal') public editContentModal: ModalDirective;
  @ViewChild('videoUrlContentModal') public videoUrlContentModal: ModalDirective;

  // showImageModal(id: number) {
  //   this.imageModal.show();
  // }

  // showVideoUploadThumbnailModal(id: number) {
  //   this.uploadVideoThumbnailModal.show();
  // }

  addImageSubmit(imageUploadForm: NgForm) {
    this.uploadImage(this.portfolioGrid);
  }

  // Video and Doc Upload Button listener
  contentUpload() {
    this.uploadForm(this.portfolioGrid);
  }

  thumbnailUploadUpdate(file) {
    this.portfolioGrid.thumbnail = file;
  }

  ngOnchanges(changes: SimpleChanges) {
    // console.log(changes);
  }

  // via true: Drag and Drop, false: File Chooser
  uploadImage(portfolioGrid: PortfolioGrid) {

    this.progressService.start();

    //Converting all arrays to string
    // Put all the constraints here before sending to upload service
    let dataToBeSent: any = {
      name: this.portfolioGrid.name,
      asset: this.portfolioGrid.asset,
      typography: (this.portfolioGrid.typography instanceof Array) ? this.portfolioGrid.typography.join("|") : undefined,
      softwares: (this.portfolioGrid.softwares instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      technologies: (this.portfolioGrid.technologies instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      color_schemes: (this.portfolioGrid.color_schemes instanceof Array) ? this.portfolioGrid.color_schemes.join("|") : undefined,
      inspiration: (this.portfolioGrid.inspiration != "" && this.portfolioGrid.inspiration != "<p></p>") ? this.portfolioGrid.inspiration : undefined,
      thumbnail: this.portfolioGrid.thumbnail,
      device: this.portfolioGrid.getDevice(),
      megapixel: this.portfolioGrid.getMegapixel(),
      aperture: this.portfolioGrid.getAperture(),
      sensor: this.portfolioGrid.getSensor(),
      resolution: this.portfolioGrid.getResolution(),
      iso: this.portfolioGrid.getIso(),
      shutterspeed: this.portfolioGrid.getShutterspeed()
    };

    this.uploadService.uploadFile(dataToBeSent).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.DownloadProgress:
          let loaded = Math.round(event.loaded);
          let total = Math.round(event.total);

          console.log(`Upload in progress! ${loaded / total}% loaded`);
        break;
        case HttpEventType.Response:
          this.progressService.done();
          if (event.body.status) {
            this.addEl(event.body.payload);
            if (event.body.payload.type == 1 || event.body.payload.type == 5) {
              this.imageModal.hide();
            }
            this.tmpData = {};
          } else {
            this.imageModal.hide();
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = event.body.message;
            this.tmpData = {};
          }
        break;
      }
    });

  }

  /*
  *   via = true from Dag and Drop/ false from File Chooser
  */
  uploadForm(portfolioGrid: PortfolioGrid) {

    this.progressService.start();

    //Converting all arrays to string
    // Put all the constraints here before sending to upload service
    let dataToBeSent: any = {
      name: this.portfolioGrid.name,
      asset: this.portfolioGrid.asset,
      typography: (this.portfolioGrid.typography instanceof Array) ? this.portfolioGrid.typography.join("|") : undefined,
      softwares: (this.portfolioGrid.softwares instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      technologies: (this.portfolioGrid.technologies instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      color_schemes: (this.portfolioGrid.color_schemes instanceof Array) ? this.portfolioGrid.color_schemes.join("|") : undefined,
      inspiration: (this.portfolioGrid.inspiration != "" && this.portfolioGrid.inspiration != "<p></p>") ? this.portfolioGrid.inspiration : undefined,
      thumbnail: this.portfolioGrid.thumbnail,
      device: this.portfolioGrid.getDevice(),
      megapixel: this.portfolioGrid.getMegapixel(),
      aperture: this.portfolioGrid.getAperture(),
      sensor: this.portfolioGrid.getSensor(),
      resolution: this.portfolioGrid.getResolution(),
      iso: this.portfolioGrid.getIso(),
      shutterspeed: this.portfolioGrid.getShutterspeed()
    };

    this.uploadService.uploadFile(dataToBeSent).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {

        case HttpEventType.DownloadProgress:
          let loaded = Math.round(event.loaded);
          let total = Math.round(event.total);

          console.log(`Upload in progress! ${loaded / total}% loaded`);
          break;
        case HttpEventType.Response:
          this.progressService.done();

          if (event.body.status) {
            this.addEl(event.body.payload);
            if (event.body.payload.type == 1) {
              this.imageModal.hide();

            } else if (event.body.payload.type == 3) {
              this.uploadVideoThumbnailModal.hide();
            } else if (event.body.payload.type == 4) {
              this.uploadDocThumbnailModal.hide();
            }
            // this.imageModalThumbnail.value = undefined;
            // this.uploadVideoThumbnailModalThumbnail.value = undefined;
            // this.uploadDocThumbnailModalThumbnail.value = undefined;
            this.tmpData = {};
          } else {
            this.imageModal.hide();
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = event.body.message;
            this.tmpData = {};
            // this.imageModalThumbnail.value = undefined;
            // this.uploadVideoThumbnailModalThumbnail.value = undefined;
            // this.uploadDocThumbnailModalThumbnail.value = undefined;
          }
          break;
      }
    });
  }

  // Open Reposition Modal
  openImageRepositionModal($event) {

    this.portfolioGrid.reset();
    let grid = $event;
    this.tmpData = {};
    this.updateRepositionImageModal.show();

    // Get Data from DB
    this.uploadService.getGrid(grid.id, false).subscribe(
      res => {

        console.log(res);
        if (res['status']) {

          // For Loading Image in Croppie, identify repo, thumb or asset content
          if (res['payload']['asset_from'] == "reposition") {
            this.tmpData['asset'] = res['payload']['reposition_image'];
          } else if (res['payload']['asset_from'] == "thumbnail") {
            this.tmpData['asset'] = res['payload']['thumbnail_image'];
          } else if (res['payload']['asset_from'] == "asset") {
            this.tmpData['asset'] = res['payload']['asset'];
          } else {
            this.tmpData['asset'] = "nothing";
          }

          this.tmpData['index'] = grid.index;

          this.tmpData['id'] = res['payload']['id'];

          // Height and Width of Viewport and Crop Image
          if (grid.width > 0) {
            this.tmpData['width'] = (document.body.clientWidth / 5.071428571428571) * grid.width;
          } else {
            this.tmpData['width'] = document.body.clientWidth / 5.071428571428571;
          }

          if (grid.height > 0) {
            this.tmpData['height'] = (document.body.clientWidth / 5.071428571428571) * grid.height;
          } else {
            this.tmpData['height'] = document.body.clientWidth / 5.071428571428571;
          }

          setTimeout(() => {

            this.croppieInstance = new Croppie(this.repositionImage.nativeElement,
              {
                viewport: { width: this.tmpData['width'], height: this.tmpData['height'] },
                boundary: { width: this.tmpData['width'], height: this.tmpData['height'] },
                enforceBoundary: true
              });

            this.croppieInstance.bind({
              url: this.tmpData['asset'],
              points: [0, 0, this.tmpData['width'], this.tmpData['height']],
              orientation: 0,
              zoom: 0,
              useCanvas: false
            });
          }, 100);

        } else {
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = res['message'];
        }
      }, err => {
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Something went wrong while Changing Reposition Content.';
      });
  }


  // Reposition Submit
  updateRepositionImageSubmit(form: NgForm) {

    setTimeout(() => {
      this.croppieInstance.result({
        type: 'blob',
        size: 'original',
        format: 'jpeg',
        quality: 1,
        circle: false
      }).then(res => {
        this.uploadService.repositionImage(this.tmpData['id'], res).subscribe(dude => {
          if (dude['status']) {
            this.widgets[this.tmpData['index']].asset = dude['payload']['reposition_image'];
            this.hideImageRepositionModal();
            // this.croppieInstance.destroy();
          }
        },
          err => {
            this.hideImageRepositionModal();
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong while Changing Reposition Content.';
          });
      });

    }, 100);
  }

  hideImageRepositionModal() {
    this.croppieInstance.destroy();
    this.updateRepositionImageModal.hide();
  }

  @ViewChild('editContentModalForm') editContentModalForm: NgForm;

  editContentModalFormSubmit(form: NgForm) {

    this.progressService.start();

    //Converting all arrays to string
    // Put all the constraints here before sending to upload service
    let dataToBeSent: any = {
      name: this.portfolioGrid.name,
      asset: this.portfolioGrid.asset,
      typography: (this.portfolioGrid.typography instanceof Array) ? this.portfolioGrid.typography.join("|") : undefined,
      softwares: (this.portfolioGrid.softwares instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      technologies: (this.portfolioGrid.technologies instanceof Array) ? this.portfolioGrid.technologies.join("|") : undefined,
      color_schemes: (this.portfolioGrid.color_schemes instanceof Array) ? this.portfolioGrid.color_schemes.join("|") : undefined,
      inspiration: (this.portfolioGrid.inspiration != undefined) ? this.portfolioGrid.inspiration : undefined,
      thumbnail: (this.portfolioGrid.thumbnail instanceof File) ? this.portfolioGrid.thumbnail : undefined,
      id: this.portfolioGrid.id,
      device: this.portfolioGrid.getDevice(),
      megapixel: this.portfolioGrid.getMegapixel(),
      aperture: this.portfolioGrid.getAperture(),
      sensor: this.portfolioGrid.getSensor(),
      resolution: this.portfolioGrid.getResolution(),
      iso: this.portfolioGrid.getIso(),
      shutterspeed: this.portfolioGrid.getShutterspeed()
    };

    if (form.value.thumbnail == undefined) {
      this.uploadService.updateThumbnailImage(dataToBeSent).subscribe(
        (res) => {
          if (res['status']) {
            this.widgets[this.tmpData['index']].name = res['payload']['name'];
            this.progressService.done();
            this.editContentModal.hide();
          } else {
            this.progressService.done();
            this.editContentModal.hide();
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = 'Something went wrong while Changing Content.';
          }
        },
        (err) => {
          this.progressService.done();
          this.editContentModal.hide();
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong while Changing Content.';
        }
      );
    } else {
      this.uploadService.updateThumbnailImage(dataToBeSent).subscribe(
        (res) => {
          if (res['status']) {
            this.widgets[this.tmpData['index']].name = res['payload']['name'];
            this.widgets[this.tmpData['index']].asset = res['payload']['thumbnail_image'];
            this.progressService.done();
            this.editContentModal.hide();
          }
        },
        (err) => {
          this.progressService.done();
          this.editContentModal.hide();
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = 'Something went wrong while Changing Content.';
        }
      );
    }
  }

  ngOnDestroy() {
    if(this.fileChooserGridData != undefined && this.fileChooserGridData != null) {
      this.fileChooserGridData.unsubscribe();
    }

    if(this.fileChooserType != undefined && this.fileChooserType != null) {
      this.fileChooserType.unsubscribe();
    }

    if(this.webCamContent != undefined && this.webCamContent != null) {
      this.webCamContent.unsubscribe();
    }
  }

  // @ViewChild('editContentModalDescText') editContentModalDescText: ElementRef;
  openEditContentModal(event) {

    this.portfolioGrid.reset();

    this.uploadService.getGrid(event.id, false).subscribe(
      res => {
        this.editContentTabs.tabs[0].active = true;

        if (res['status']) {
          this.tmpData['index'] = event['index'];
          this.tmpData['showEditContentModal'] = true;

          this.portfolioGrid.name = res['payload']['name'];

          if (res['payload']['typography'] != null && res['payload']['typography'] != undefined && res['payload']['typography'] != "") {
            this.portfolioGrid.typography = res['payload']['typography'].split('|');
            this.portfolioGrid.typography.forEach((item, index) => {
              this.typographyData.push({
                id: item,
                text: item
              });
            });
          }

          if (res['payload']['softwares'] != null && res['payload']['softwares'] != undefined && res['payload']['softwares'] != "") {
            this.portfolioGrid.softwares = res['payload']['softwares'].split('|');
            this.portfolioGrid.softwares.forEach((item, index) => {
              this.softwaresData.push({
                id: item,
                text: item
              });
            });
          }

          if (res['payload']['technologies'] != null && res['payload']['technologies'] != undefined && res['payload']['technologies'] != "") {
            this.portfolioGrid.technologies = res['payload']['technologies'].split('|');
            this.portfolioGrid.technologies.forEach((item, index) => {
              this.technologiesData.push({
                id: item,
                text: item
              });
            });
          }

          if (res['payload']['color_schemes'] != null && res['payload']['color_schemes'] != undefined && res['payload']['color_schemes'] != "") {
            this.portfolioGrid.color_schemes = res['payload']['color_schemes'].split('|');

            this.portfolioGrid.color_schemes.forEach((item, index) => {
              this.colorSchemesData.push({
                id: item,
                text: item
              });
            });
          }

          if (res['payload']['inspiration'] != null && res['payload']['inspiration'] != undefined) {
            this.portfolioGrid.inspiration = res['payload']['inspiration'];
          }

          if (res['payload']['device'] != null && res['payload']['device'] != undefined) {
            this.portfolioGrid.setDevice(res['payload']['device']);
          }

          if (res['payload']['megapixel'] != null && res['payload']['megapixel'] != undefined) {
            this.portfolioGrid.setMegapixel(res['payload']['megapixel']);
          }

          if (res['payload']['aperture'] != null && res['payload']['aperture'] != undefined) {
            this.portfolioGrid.setAperture(res['payload']['aperture']);
          }

          if (res['payload']['sensor'] != null && res['payload']['sensor'] != undefined) {
            this.portfolioGrid.setSensor(res['payload']['sensor']);
          }

          if (res['payload']['resolution'] != null && res['payload']['resolution'] != undefined) {
            this.portfolioGrid.setResolution(res['payload']['resolution']);
          }

          if (res['payload']['iso'] != null && res['payload']['iso'] != undefined) {
            this.portfolioGrid.setIso(res['payload']['iso']);
          }

          if (res['payload']['shutterspeed'] != null && res['payload']['shutterspeed'] != undefined) {
            this.portfolioGrid.setShutterspeed(res['payload']['shutterspeed']);
          }

          this.portfolioGrid.id = +res['payload']['id'];

          this.editContentModal.show();
          this.cdr.detectChanges();
        } else {
          this.editContentModal.hide();
          this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          this.bsModalRef.content.message = res['message'];
        }
      }, err => {
        this.editContentModal.hide();
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Something went wrong while Loading Content.';
      }
    );
  }

  hideEditContentModal() {
    this.progressService.done();
    this.editContentModal.hide();
  }

  hideImageModal() {
    this.progressService.done();
    this.imageModal.hide();
  }

  hideUploadVideoThumbnailModal() {
    this.progressService.done();
    this.uploadVideoThumbnailModal.hide();
  }

  hideUploadDocThumbnailModal() {
    this.progressService.done();
    this.uploadDocThumbnailModal.hide();
  }

  @ViewChild('confirmDeleteModal') confirmDeleteModal: ModalDirective;
  @ViewChild('confirmDeleteModalIndex') confirmDeleteModalIndex: ElementRef;

  openConfirmDeleteModal(index: number) {
    this.confirmDeleteModalIndex.nativeElement.value = index;
    this.confirmDeleteModal.show();
  }

  hideConfirmDeleteModal() {
    this.confirmDeleteModal.hide();
  }

  typography_list = ['Tahoma', 'Betram Let', 'Times New Roman', 'Ubuntu', 'Ubuntu Mono', 'Ubuntu Condensed', 'Monaco', 'Comic Sans', 'Courier', 'Consolas'];
  softwares_list = ["MS Paint", "Paint", "Corel Draw", "Adobe Photoshop", "Adobe Illustrator", "Inkscape", "GIMP", "Sketch Book"];

  // Advance Section of Modal
  public typographyData: Array<Select2OptionData> = [];
  public softwaresData: Array<Select2OptionData> = [];
  public technologiesData: Array<Select2OptionData> = [];
  public colorSchemesData: Array<Select2OptionData> = [];
  public options: Select2Options;
  public current: string;
  public colorSchemeOptions: Select2Options;

  ngOnInit() {

    this.typography_list.forEach(element => {
      this.typographyData.push({
        id: element,
        text: element
      });
    });

    this.softwares_list.forEach(element => {
      this.softwaresData.push({
        id: element,
        text: element
      });
    });

    this.options = {
      multiple: true,
      tags: true
    }

    this.colorSchemeOptions = {
      multiple: true,
      tags: true,
      templateSelection: this.formatSelection
    }
  }

  select2changed(data, el, key) {
    this.portfolioGrid[key] = data.value;
  }

  richTxtEditorChanged(dataValue, el, key) {
    this.portfolioGrid[key] = (dataValue != '' && dataValue != undefined) ? dataValue.value : el[key];
  }

  formatSelection(state) {
    if (!state.id) {
      return state.text;
    }

    let $state = $(
      '<span class="edit-color-scheme-tags" style="background:' + state.text + '">' + '</span>'
    );
    return $state;
  };

  hideVideoUrlModal() {
    this.videoUrlContentModal.hide();
  }

  videoUrlSubmit(form: NgForm) {
    this.uploadVideoUrl(this.portfolioGrid);
  }

  // via true: Drag and Drop, false: File Chooser
  uploadVideoUrl(portfolioGrid: PortfolioGrid) {

    this.progressService.start();

    //Converting all arrays to string
    // Put all the constraints here before sending to upload service
    let dataToBeSent: any = {
      name: this.portfolioGrid.name,
      asset: this.portfolioGrid.asset,
      typography: (this.portfolioGrid.typography instanceof Array) ? this.portfolioGrid.typography.join("|") : undefined,
      softwares: (this.portfolioGrid.softwares instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      technologies: (this.portfolioGrid.technologies instanceof Array) ? this.portfolioGrid.softwares.join("|") : undefined,
      color_schemes: (this.portfolioGrid.color_schemes instanceof Array) ? this.portfolioGrid.color_schemes.join("|") : undefined,
      inspiration: (this.portfolioGrid.inspiration != "" && this.portfolioGrid.inspiration != "<p></p>") ? this.portfolioGrid.inspiration : undefined,
      thumbnail: this.portfolioGrid.thumbnail
    };
    console.log('dodo : ', dataToBeSent);
    this.uploadService.uploadVideoUrl(dataToBeSent).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.DownloadProgress:
          let loaded = Math.round(event.loaded);
          let total = Math.round(event.total);

          console.log(`Upload in progress! ${loaded / total}% loaded`);
          break;
        case HttpEventType.Response:
          this.progressService.done();
          if (event.body.status) {
            this.addEl(event.body.payload);
            if (event.body.payload.type == 2) {
              this.videoUrlContentModal.hide();
            }
          } else {
            this.videoUrlContentModal.hide();
            this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
            this.bsModalRef.content.message = event.body.message;
          }
          break;
      }
    });
  }

  @ViewChild('contentDetailsCard', { read: TemplateRef }) contentDetailsCard: TemplateRef<any>;
  @ViewChild('contentDetailsCardContainer', { read: ViewContainerRef }) contentDetailsCardContainer: ViewContainerRef;

  currentContentIndex: number;

  showContentDetailsCard(event: any) {
    this.currentContentIndex = event.index;
    this.contentDetailsCardContainer.createEmbeddedView(this.contentDetailsCard);
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'left', event.posX + 'px');
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'top', event.posY + 'px');
    this.el.nativeElement.querySelector('.content-details-card').focus();
  }

  hideContentDetailsCard(event: number) {
    this.contentDetailsCardContainer.clear();
  }

  isContentDetails() {

    if ((this.widgets[this.currentContentIndex].typography == undefined || this.widgets[this.currentContentIndex].typography == "") &&
      (this.widgets[this.currentContentIndex].softwares == undefined || this.widgets[this.currentContentIndex].softwares == "") &&
      (this.widgets[this.currentContentIndex].technologies == undefined || this.widgets[this.currentContentIndex].technologies == "") &&
      (this.widgets[this.currentContentIndex].color_schemes == undefined || this.widgets[this.currentContentIndex].color_schemes == "") &&
      (this.widgets[this.currentContentIndex].inspiration == undefined || this.widgets[this.currentContentIndex].inspiration == "" || this.widgets[this.currentContentIndex].inspiration == "<p></p>")) {
      return false;
    }

    return true;
  }

  @ViewChild('resizeAction') resizeAction: ElementRef;
  @ViewChild('editAction') editAction: ElementRef;
  @ViewChild('delAction') delAction: ElementRef;

  gridClickForMobile(event) {

    if(window.innerWidth < 425) {
      this.currentSelectedActionForMobile = event;
      if(this.resizeAction) {
        this.ren.removeClass(this.resizeAction.nativeElement, 'displayNone');
      }

      if(this.editAction) {
        this.ren.removeClass(this.editAction.nativeElement, 'displayNone');
      }

      if(this.delAction) {
        this.ren.removeClass(this.delAction.nativeElement, 'displayNone');
      }
    }
  }

  isRepositionShown(type) {
    if(type == 1 || type == 3 || type == 4) {
      return true;
    } else {
      return false;
    }

  }

  generalMobileFileUpload(data) {
    this.portfolioGrid.reset();
    let extension = data.name.split('.').pop();
    if (this.supportedImageFormats.includes(extension)) {
      this.portfolioGrid.asset = data;
      this.showRespectiveModal(1);
    } else if (this.supportedVideoFormats.includes(extension)) {
      this.portfolioGrid.asset = data;
      this.showRespectiveModal(3);
    } else if(this.supportedDocFormats.includes(extension)) {
      this.portfolioGrid.asset = data;
      this.showRespectiveModal(4);
    } else if(this.supportedGifFormats.includes(extension)) {
      this.portfolioGrid.asset = data;
      this.showRespectiveModal(1);
    } else {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'File Type not recognized.';
    }
  }

  isGridLocked: boolean = false;

  lockUnlockGrid() {
    this.isGridLocked = !this.isGridLocked;

    if(window.innerWidth < 425) {
      setTimeout( () => {
        this.gridster
            .setOption('dragAndDrop', this.isGridLocked)
            .setOption('resizable', this.isGridLocked)
            .setOption('direction', this.isGridLocked ? 'vertical': 'none')
            .setOption('floating', this.isGridLocked)
            .reload();
      });
    }
  }
}
