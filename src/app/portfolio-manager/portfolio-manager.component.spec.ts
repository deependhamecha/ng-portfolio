import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioManagerComponent } from './portfolio-manager.component';

describe('ManualGridComponent', () => {
  let component: PortfolioManagerComponent;
  let fixture: ComponentFixture<PortfolioManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
