import {    Component,
            OnInit,
            ViewChild,
            AfterViewInit,
            Output,
            EventEmitter,
            ElementRef,
            OnDestroy,
            Inject
} from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'acdn-web-cam-modal',
  templateUrl: './web-cam-modal.component.html',
  styleUrls: ['./web-cam-modal.component.css']
})
export class WebCamModalComponent implements OnInit, OnDestroy, AfterViewInit {

    @Output() getImageEmit: EventEmitter<any> = new EventEmitter<any>();

    // Web Cam Modal
    @ViewChild('webCamModal') webCamModal: ModalDirective;

    constructor(private el: ElementRef) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.showWebCamModal();
    }

    showWebCamModal() {
        this.webCamModal.show();
    }

    hideWebcamModal() {
        this.webCamModal.hide();
    }

    getImage(event) {
        this.getImageEmit.emit(event);
    }

    ngOnDestroy() {
        this.getImageEmit.unsubscribe();
    }
}
