import {  Component,
          OnInit,
          Input,
          Output,
          EventEmitter,
          ElementRef,
          Renderer2,
          ViewChild,
          AfterViewInit
} from '@angular/core';

import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { PortfolioProfile } from '../portfolio_profile.model';
import { UploadService } from '../services/upload.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @Input() navToggleVal;
  @Input() isPreview: boolean = false;
  @Input() isShownForDesktop: boolean;
  @Output('navToggle') navToggleEmit = new EventEmitter();
  @Output('optionsMenu') optionsMenuEmit = new EventEmitter();

  email: string;
  password: string;

  _portfolioProfile: any;

  @Input('portfolioProfile')
  set portfolioProfile(value: any) {
    this._portfolioProfile = value;
  }

  get portfolioProfile() {
    return this._portfolioProfile;
  }

  // @Input('portfolioProfile') portfolioProfile: any;

  isSharemenuActive = false;
  isOptionsMenuActive = false;

  isCookieSet:boolean = false;

  constructor(private el: ElementRef,
              private ren: Renderer2,
              private r: Router,
              private cookieService: CookieService,
              private uploadService: UploadService) {

    if( this.cookieService.get('_token') !== undefined &&
        this.cookieService.get('_token') !== null &&
        this.cookieService.get('_token') !== '') {
      this.isCookieSet = true;
    }


  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if((!this.isPreview) && window.innerWidth < 425) {
      this.ren.addClass(this.el.nativeElement.querySelector('.backToEditWrapper'), 'displayNone');
    }
  }

  navToggle() {
    this.navToggleEmit.emit();
  }

  // Deprecated
  optionsMenu() {
    this.optionsMenuEmit.emit();
  }


  openShareIconsList() {
    if(this.isSharemenuActive) {
      this.isSharemenuActive = !this.isSharemenuActive;
      this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'opacity', '');
      this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'transform', '');
    } else {
      this.isSharemenuActive = !this.isSharemenuActive;
      this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'opacity', '1');
      this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'transform', 'rotate3d(1,1,0,0deg)');
      this.el.nativeElement.querySelector('.share-icons-contents').focus();
    }
  }

  shareMenuOnBlur() {
    this.isSharemenuActive = !this.isSharemenuActive;
    this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'opacity', '');
    this.ren.setStyle(this.el.nativeElement.querySelector('.share-icons-contents'), 'transform', '');
  }

  openOptionsList() {
    if(this.isOptionsMenuActive) {
      this.isOptionsMenuActive = !this.isOptionsMenuActive;
      this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'opacity', '');
      this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'transform', '');
    } else {
      this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'opacity', '1');
      this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'transform', 'rotate3d(1,1,0,0deg)');
      this.el.nativeElement.querySelector('.options-contents').focus();
      this.isOptionsMenuActive = !this.isOptionsMenuActive;
    }
  }

  optionsMenuOnBlur() {
    this.isOptionsMenuActive = !this.isOptionsMenuActive;
    this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'opacity', '');
    this.ren.setStyle(this.el.nativeElement.querySelector('.options-contents'), 'transform', '');
  }

  routeToEditPage() {
    this.r.navigate(['/']);
  }

  logout() {
    this.uploadService.logout().subscribe( (res) => {
      if(res['status'] == true) {
        this.cookieService.delete('_token', '/');
        this.cookieService.delete('laravel_session', '/');
        this.cookieService.delete('XSRF-TOKEN', '/');
        this.isCookieSet = false;
      }
    }, (err) => {
      this.cookieService.delete('_token', '/');
      this.cookieService.delete('laravel_session', '/');
      this.cookieService.delete('XSRF-TOKEN', '/');
      document.location.href = "https://acadnion.com";
    });
  }

  @ViewChild('emailError') emailError: ElementRef;

  login(loginForm) {
    this.uploadService.login(loginForm.value.email, loginForm.value.password).subscribe( (res) => {
      if(res['status'] === true) {
        // console.log('TOKEN : ', res['payload']['_token']);
        this.cookieService.set('_token', res['payload']['_token']+"", parseInt(res['payload']['validity']), '/' );
        this.isCookieSet = true;
      } else {
        console.log('TOKEN : ', res['message']);
        this.emailError.nativeElement.innerHTML = res['message'];
      }
      $('#new-login-signup-modal').modal('hide');
    }, (err) => {
      $('#new-login-signup-modal').modal('hide');
      // this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      // this.bsModalRef.content.message = 'Something went wrong while Login.';
    });
  }
}
