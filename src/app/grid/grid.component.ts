import {  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  AfterViewInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Renderer2
} from '@angular/core';

import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { UploadService } from '../services/upload.service';
import { Subject } from 'rxjs/Subject';

import { GridsterItemComponent } from 'angular2gridster';

import Gifffer from 'gifffer';

@Component({
selector: 'app-grid',
templateUrl: './grid.component.html',
styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, AfterViewInit {

@Input('name') name : string;
@Input('asset') asset : string;
@Input('indexNumber') indexNumber : number;
@Input('id') id: number;
@Input('width') width: number;
@Input('height') height: number;
@Input('type') type: number;

_typography: string;
@Input('typography')
set typography(value: string) {
this._typography = value.split("|").join(", ");
}

get typography(): string {
return this._typography;
}

_softwares: string;
@Input('softwares')
set softwares(value: string) {
this._softwares = value.split("|").join(", ");
}

get softwares(): string {
return this._softwares;
}

_technologies: string;
@Input('technologies')
set technologies(value: string) {
this._technologies = value.split("|").join(", ");
}

get technologies(): string {
return this._technologies;
}

_color_schemes: string;
@Input('colorSchemes')
set color_schemes(value: string) {
this._color_schemes = value.split("|").join(", ");
}

get color_schemes(): string {
return this._color_schemes;
}

@Input('inspiration') inspiration: string;

@Output('gridClickForMobile') gridClickForMobileEmit: EventEmitter<any> = new EventEmitter<any>();

isShownForDesktop: boolean = true;

// @ViewChild(GridComponent) gc: GridComponent;
@ViewChild('eachGridWrapper') eachGridWrapper: ElementRef;

bsModalRef: BsModalRef;

constructor(private modalService: BsModalService,
      private uploadService: UploadService,
      private ren: Renderer2,
      private el: ElementRef) { }

ngOnInit() {
}

// @ViewChild(GridsterItemComponent) gridItem: GridsterItemComponent;

ngAfterViewInit() {
  // console.log("gridItem : "+this.gc);
  // console.log(this.type);
  this.startGifffer();
}

// openImageEditModal() {
//   this.uploadService.getGrid(this.id, false).subscribe(
//     res => {
//       console.log('getGrid Response : ', res);
//       this.bsModalRef = this.modalService.show(OpenImageEditModalComponent);
//       this.bsModalRef.content.title = 'Edit your Image';
//       this.bsModalRef.content.isChanged = false;
//       this.bsModalRef.content.name = res['payload']['name'];
//       this.bsModalRef.content.id = res['payload']['id'];
//       this.bsModalRef.content.asset = res['payload']['asset'];
//       this.bsModalRef.content.type = res['payload']['type'];
//   }, err => {
//       console.log(err);
//   });
// }

ngOnChanges(changes: SimpleChanges) {
// console.log('GRID CHANGES : ', changes);
}

@Output('openImageRepositionModal') openImageRepositionModal = new EventEmitter<any>();

openRepositionModal() {
  this.openImageRepositionModal.emit({
    id : this.id,
    name: this.name,
    asset: this.asset,
    index: this.indexNumber,
    width: this.width,
    height: this.height,
    type: this.type
  });
}

@Output('openEditContentModal') openEditContentModal = new EventEmitter<any>();

openThumbnailEditContentModal() {
  this.openEditContentModal.emit({
    id : this.id,
    name: this.name,
    asset: this.asset,
    index: this.indexNumber,
    width: this.width,
    height: this.height,
    type: this.type
  });
}

@Output('openConfirmDeleteModal') openConfirmDeleteModal = new EventEmitter<number>();

openDeleteModal(index: number) {
  this.openConfirmDeleteModal.emit(index);
}

@Output('showContentDetailsCard') showContentDetailsCardEmit: EventEmitter<any> = new EventEmitter<any>();

showContentDetailsCard(event) {
  if( this.isContentDetails() ) {
    this.showContentDetailsCardEmit.emit({index: this.indexNumber, posX: event.clientX , posY: event.clientY});
  }
}


isContentDetails() {

  if( (this.typography == undefined || this.typography == "") &&
      (this.softwares == undefined || this.softwares == "") &&
      (this.technologies == undefined || this.technologies == "") &&
      (this.color_schemes == undefined || this.color_schemes == "") &&
      (this.inspiration == undefined || this.inspiration == "" || this.inspiration == "<p></p>") ) {
    return false;
  }

  return true;
}

  isContentDetailsAvailable() {
    if( this.isContentDetails() ) {
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-toggle');
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-placement');
      this.ren.removeAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'title');
    } else {
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-toggle', 'tooltip');
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'data-placement', 'top');
      this.ren.setAttribute(this.el.nativeElement.querySelector('.show-information-icon'), 'title', 'No Content Available!');
    }
  }

  gifs;

  public startGifffer() {

    this.gifs = Gifffer({
      playButtonStyles: {
        'width': '60px',
        'height': '60px',
        'border-radius': '150%',
        'border': '3px solid rgba(255, 255, 255, 0.5)',
        'position': 'absolute',
        'top': '50%',
        'left': '50%',
        'margin': '-30px 0 0 -30px',
        "font-size": "22px",
        "color": "#fff",
        "letter-spacing": "1px",
        "display": "flex",
        "-webkit-box-align": "center",
        "-ms-flex-align": "center",
        "align-items": "center",
        "-webkit-box-pack": "center",
        "-ms-flex-pack": "center",
        "justify-content": "center"
      },
      playButtonIconStyles: {}
    });

  }

  // mobileActive: boolean = false;

  gridClickForMobile(event) {
    // this.mobileActive = true;

    this.gridClickForMobileEmit.emit({
      id : this.id,
      name: this.name,
      asset: this.asset,
      index: this.indexNumber,
      width: this.width,
      height: this.height,
      type: this.type
    });
  }


}
