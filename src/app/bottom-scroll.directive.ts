import {    Directive,
            HostListener,
            Output,
            EventEmitter
        } from '@angular/core';

@Directive({
selector: '[bottomScroll]'
})
export class BottomScrollDirective {

    @Output('bottomScrolled')
    bottomScrolled: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        
    }

    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        //In chrome and some browser scroll is given to body tag
        // let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + window.innerHeight;
        let max = document.documentElement.scrollHeight;
        // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
        if(pos == max )   {
            this.bottomScrolled.emit();
        }
    }



}