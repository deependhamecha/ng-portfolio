import {    Component,
            EventEmitter,
            OnInit,
            Input,
            Output,
            AfterViewInit
        } from '@angular/core';

import {WebCamComponent} from 'ack-angular-webcam';
import {Http, Request} from '@angular/http';

declare function unescape(s:string): string;

@Component({
  selector: 'app-acdn-web-cam',
  templateUrl: './acdn-web-cam.component.html',
  styleUrls: ['./acdn-web-cam.component.css']
})
export class AcdnWebCamComponent implements OnInit, AfterViewInit {

    webcam: WebCamComponent; //will be populated by <ack-webcam [(ref)]="webcam">

    @Output('getImage') getImageEmit: EventEmitter<any> = new EventEmitter<any>();

    options = {
        audio: false,
        video: true,
        width: innerWidth*0.78,
        height: innerHeight*0.75,
        fallbackMode: 'callback',
        fallbackSrc: 'jscam_canvas_only.swf',
        fallbackQuality: 85,
        cameraType: 'front'
    };
    constructor(public http: Http) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }

    getImage() {
        console.log('GET IMAGE');
        this
            .webcam
            .getBase64()
            .then( base=> {
                this.getImageEmit.emit({status: true, file: this.dataURItoBlob(base)});
            })
            .catch( e=>{
                this.getImageEmit.emit(undefined);
            });
    }

    onCamError(err) {
        console.log('Cam Err');
        this.getImageEmit.emit(undefined);
    }

    onCamSuccess(event) {
        console.log('SUCCESSFULLY WEBCAM');
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new File([new Blob([ia], {type: mimeString})], 'acdnWebCam.png', {type: mimeString, lastModified: Date.now()});
    }

}
