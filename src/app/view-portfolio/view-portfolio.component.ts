import {  Component,
          OnInit,
          ElementRef,
          ViewChild,
          AfterViewInit,
          TemplateRef,
          ViewContainerRef,
          Renderer2,
          Input,
          Inject
} from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { CommonErrorModalComponent } from '../common-error-modal/common-error-modal.component';
import { UploadService } from '../services/upload.service';
import { GridsterComponent, IGridsterOptions, GridsterItemComponent } from 'angular2gridster';
import { OnDestroy } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { PortfolioProfile } from '../portfolio_profile.model';

@Component({
  selector: 'app-view-portfolio',
  templateUrl: './view-portfolio.component.html',
  styleUrls: ['./view-portfolio.component.css']
})
export class ViewPortfolioComponent implements AfterViewInit, OnDestroy {

  @ViewChild('gridLoader') gridLoader: ElementRef;
  @ViewChild(GridsterComponent) gridster: GridsterComponent;

  isActive = false;
  navToggleVal = false;
  isShownForDesktop: boolean = false;
  bsModalRef: BsModalRef;
  widgets: Array<any> = [];
  optionsMenuToggle = false;
  isContactMobileWrapperShown: boolean = false;

  // Used For Mobile Only
  cellHeight: number;

  gridsterOptions: IGridsterOptions = {
    lanes: 4, // how many lines (grid cells) dashboard has
    direction: 'vertical', // items floating direction: vertical/horizontal
    widthHeightRatio : 1,
    shrink: true, // Shirnks vertical space. Check bottom after deleting one content
    responsiveView: true,
    resizable: false, // possible to resize items by drag n drop by item edge/corner
    dragAndDrop: false, // possible to change items position by drag n drop
    useCSSTransforms: true,
    floating: true
  };

  lastWindowWidth: number;

  portfolioProfile: PortfolioProfile;

  constructor(private route: ActivatedRoute,
              private modalService: BsModalService,
              private uploadService: UploadService,
              private el: ElementRef,
              private ren: Renderer2,
              private r: Router) {
    this.maxHeightOfScreen = window.innerHeight+'px';
    // this.lastWindowWidth = window.innerWidth;
    if(window.innerWidth >= 425) {
      // this.gridsterOptions.lanes = 4;
      this.isShownForDesktop = true;
      this.isContactMobileWrapperShown = false;
    }
     else {
    //   this.gridsterOptions.lanes = 1;
      this.isShownForDesktop = false;
      this.isContactMobileWrapperShown = true;
    }
  }

  username: string;

  ngAfterViewInit() {
    this.route.params.subscribe( params => {
      console.log(params['username']);
      this.username = params['username'];
    });
    this.loadGrid();
  }

  _loadGridSub;
  mobileOptions;

  loadGrid() {
    console.log('username : ', this.username);
    this._loadGridSub = this.uploadService.loadViewGrid(this.username).subscribe(
      (res) => {
        if(res['status']) {
          this.widgets = [];

          let gridElements = res['payload'];
          gridElements.forEach((element, i) => {
            this.addEl(element);
          });

          // For Drag and Drop disable for view page
          setTimeout(() => {
            this.gridster.setOption('dragAndDrop', false).reload();
          });

          if(window.innerWidth < 425) {
            this.gridster.setOption('cellHeight', (window.innerWidth/4)).reload();
          }
        } else {

          // Invalid Username
          this.r.navigate(['/']);

          // this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
          // this.bsModalRef.content.message = 'Invalid Username';
        }

        // Grid Loader Hide After Loading Grid Elements
        this.gridLoader.nativeElement.style.display="none";
      },

      (err) => {
        this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
        this.bsModalRef.content.message = 'Please refresh the page as there has been an error while loading content.';
    });
  }

  mobileViewContentCounter: number = 0;

  /*
  **  Add Element(Content) to Grid
  */
  addEl(data) {

    let el;

    if(data == undefined || Object.keys(data).length < 1) {
      this.bsModalRef = this.modalService.show(CommonErrorModalComponent);
      this.bsModalRef.content.message = 'Something went wrong while Loading Content.';
    } else {

      let show = '';

      if(data.asset_from == "reposition") {
        show = data.reposition_image;
      } else if(data.asset_from == "thumbnail") {
        show = data.thumbnail_image;
      } else if(data.asset_from == "asset") {
        show = data.asset;
      } else {
        show = "nothing";
      }

      // if(window.innerWidth >= 425) {
      el = {
        x: data.x_cord,
        y: data.y_cord,
        w: data.width,
        h: data.height,
        dragAndDrop: true,
        resize: true,
        id: data.id,
        name: data.name,
        asset: data.asset,
        type: data.type,
        typography: data.typography,
        softwares: data.softwares,
        technologies: data.technologies,
        color_schemes: data.color_schemes,
        inspiration: data.inspiration,
        device: data.device,
        megapixel: data.megapixel,
        aperture: data.aperture,
        sensor: data.sensor,
        resolution: data.resolution,
        iso: data.iso,
        shutterspeed: data.shutterspeed,
        show: show
      };
      // } else {

        // let _wid;
        // let _hgt;

        // if(window.innerWidth < 425) {
        //   if(data.width > data.height) {
        //     _wid = 1;
        //     _hgt =
        //   } else {

        //   }
        // }
      //   el = {
      //     x: 0,
      //     y: this.mobileViewContentCounter,
      //     w: 1,
      //     h: data.height,
      //     dragAndDrop: true,
      //     resize: true,
      //     id: data.id,
      //     name: data.name,
      //     asset: data.asset,
      //     type: data.type,
      //     typography: data.typography,
      //     softwares: data.softwares,
      //     technologies: data.technologies,
      //     color_schemes: data.color_schemes,
      //     inspiration: data.inspiration,
      //     device: data.device,
      //     megapixel: data.megapixel,
      //     aperture: data.aperture,
      //     sensor: data.sensor,
      //     resolution: data.resolution,
      //     iso: data.iso,
      //     shutterspeed: data.shutterspeed,
      //     show: show
      //   };
      // }

      this.widgets.push(el);
      // this.mobileViewContentCounter = this.mobileViewContentCounter+data.height;
      return (this.widgets.length - 1);
    }
  }

  optionsMenu() {
    this.optionsMenuToggle = !this.optionsMenuToggle;
  }

  navToggle() {
    if(window.innerWidth < 425) {
      this.isContactMobileWrapperShown = ! this.isContactMobileWrapperShown;
    }
    this.navToggleVal = !this.navToggleVal;
  }

  @ViewChild('contentDetailsCard', {read: TemplateRef}) contentDetailsCard: TemplateRef<any>;
  @ViewChild('contentDetailsCardContainer', {read: ViewContainerRef}) contentDetailsCardContainer : ViewContainerRef;

  currentContentIndex: number;

  showContentDetailsCard(event: any) {
    this.currentContentIndex = event.index;

    this.contentDetailsCardContainer.createEmbeddedView(this.contentDetailsCard);

    setTimeout(() => {
      this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'opacity', 1);
    }, 200);
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'left', event.posX+'px');
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'top', event.posY+'px');
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'width', '400px');
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'height', 'auto');

    this.el.nativeElement.querySelector('.content-details-card').focus();
  }

  hideContentDetailsCard(event: number) {
    this.ren.setStyle(this.el.nativeElement.querySelector('.content-details-card'), 'opacity', 0);
    setTimeout( () => {

      this.contentDetailsCardContainer.clear();
    }, 500);
  }

  isContentDetails() {

    if( (this.widgets[this.currentContentIndex].typography == undefined || this.widgets[this.currentContentIndex].typography == "") &&
        (this.widgets[this.currentContentIndex].softwares == undefined || this.widgets[this.currentContentIndex].softwares == "") &&
        (this.widgets[this.currentContentIndex].technologies == undefined || this.widgets[this.currentContentIndex].technologies == "") &&
        (this.widgets[this.currentContentIndex].color_schemes == undefined || this.widgets[this.currentContentIndex].color_schemes == "") &&
        (this.widgets[this.currentContentIndex].inspiration == undefined || this.widgets[this.currentContentIndex].inspiration == "" || this.widgets[this.currentContentIndex].inspiration == "<p></p>") &&
        (this.widgets[this.currentContentIndex].device == undefined || this.widgets[this.currentContentIndex].device == "") &&
        (this.widgets[this.currentContentIndex].megapixel == undefined || this.widgets[this.currentContentIndex].megapixel == "") &&
        (this.widgets[this.currentContentIndex].aperture == undefined || this.widgets[this.currentContentIndex].color_schemes == "") &&
        (this.widgets[this.currentContentIndex].sensor == undefined || this.widgets[this.currentContentIndex].sensor == "") &&
        (this.widgets[this.currentContentIndex].resolution == undefined || this.widgets[this.currentContentIndex].resolution == "") &&
        (this.widgets[this.currentContentIndex].iso == undefined || this.widgets[this.currentContentIndex].iso == "") &&
        (this.widgets[this.currentContentIndex].shutterspeed == undefined || this.widgets[this.currentContentIndex].shutterspeed == "")) {
      return false;
    }

    return true;
  }

  ngOnDestroy() {
    console.log('comng to ddeeestroy');
    if(this._loadGridSub != undefined && this._loadGridSub != null) {
      this._loadGridSub.unsubscribe();
    }
  }

  isActualAsset: boolean = false;
  maxHeightOfScreen: string;

  @ViewChild('actualAssetModal') public actualAssetModal: ModalDirective;

  showActualAsset(event) {
      this.currentContentIndex = event;
      this.maxHeightOfScreen = (window.innerHeight)+'px';
      this.isActualAsset = true;
      this.ren.removeClass(this.el.nativeElement.querySelector('.actualAssetModal'), 'zoomOut');
      this.ren.addClass(this.el.nativeElement.querySelector('.actualAssetModal'), 'zoomIn');
      setTimeout( () => {
        this.actualAssetModal.show();
      }, 200);
    }

    hideActualAsset(event?: any) {
      this.currentContentIndex = event;
      this.ren.removeClass(this.el.nativeElement.querySelector('.actualAssetModal'), 'zoomIn');
      this.ren.addClass(this.el.nativeElement.querySelector('.actualAssetModal'), 'zoomOut');
      setTimeout( () => {
        this.isActualAsset = false;
        this.actualAssetModal.hide();
      }, 400);
  }

  onWindowResize(event) {

    // if(window.innerWidth >= 425 && this.lastWindowWidth < 425) {

      // this.loadGrid();

      // For Preview Button in Header
      // this.isShownForDesktop = true;

      // setTimeout(() => {
      //   this.gridster.setOption('dragAndDrop', false).reload();
      // }, 5000);
    // } else if(window.innerWidth < 425 && this.lastWindowWidth >= 425) {
    //   this.gridsterOptions.lanes = 1;
    //   this.loadGrid();

    //   // For Preview Button in Header
    //   this.isShownForDesktop = false;

    //   setTimeout(() => {
    //     this.gridster.setOption('dragAndDrop', false).reload();
    //   }, 1000);
    // }
    // this.lastWindowWidth = window.innerWidth;
  }

  @ViewChild('contentDetailsForMobile')
  contentDetailsForMobile: ModalDirective;

  @ViewChild('contentDetailsForMobileTmp', {read: TemplateRef})
  contentDetailsForMobileTmp: TemplateRef<any>;

  @ViewChild('_contentDetailsForMobileContainer', {read: ViewContainerRef})
  _contentDetailsForMobileContainer: ViewContainerRef;

  showContentDetailsForMobile(event) {
    this.currentContentIndex = event;
    let dude = this._contentDetailsForMobileContainer.createEmbeddedView(this.contentDetailsForMobileTmp);


    setTimeout( () => {
      this.contentDetailsForMobile.show();
    });
  }

  hideContentDetailsForMobile() {
    this.contentDetailsForMobile.hide();
  }

  @ViewChild('toast') toast: ElementRef;

  copyToClipboard(data) {
    this.ren.setStyle(this.toast.nativeElement, 'bottom', '50px');
    setTimeout( () => {
      this.ren.setStyle(this.toast.nativeElement, 'bottom', '');
    }, 3000);
  }

  /**
  *   Update Portfolio Profile
  */
  updatePortfolioProfile(data) {
    this.portfolioProfile = data;
  }
}
